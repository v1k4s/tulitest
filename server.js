/**
 * Created by Vikas Sharma on 7/8/15.
 */

var Hapi = require('hapi');
var plugins = require('./plugins');
var APIRoutes = require('./routes');
var Config = require('./config');
var Server = new Hapi.Server();
var Mongoose = require('mongoose');
var Validation = require('./utilities/validationFunctions');
var Jwt = require('jsonwebtoken');
var Bootstrap = require('./utilities/bootstrap')();

var db = Mongoose.connection;
Mongoose.connect(Config.db.test);
db.once('open',function(err){
    if(!err) console.log('Connection Successful');
});


process.env.NODE_ENV = 'dev';



if (process.env.NODE_ENV == 'test') {
    Server.connection({
        host : Config.server.host,
        port: Config.server.port.test
    });
} else {
    Server.connection({
        host : Config.server.host,
        port: Config.server.port.dev
    });
}
Server.register(plugins.pluginsArray, function (err) {
    if (err) {
        throw err;
    }
    /**
     * Start Server
     **/
    Server.start(function () {
        Server.log('info', 'Server running at: ' + Server.info.uri);
    });
});

// Authentication Strategies

Server.auth.strategy('token','jwt',{
    key: Config.secret.key,
    validateFunc: Validation.tokenValidation
});


// API Routes
var routes = APIRoutes.routes;
routes.forEach(function (routeAPI) {
    Server.route(routeAPI);
});

// Add the route
Server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply('Welcome to TuLi server!');
    }
});

Server.on('response', function (request) {
    console.log('Request payload:', request.payload);
    console.log('Response payload:', request.response.source);
});
