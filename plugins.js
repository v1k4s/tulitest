/**
 * Created by harekam on 4/30/15.
 */
var mongoConfig = require('./config');
var pack = require('./package'),
    swaggerOptions = {
        apiVersion: pack.version,
        pathPrefixSize: 2
    }
var goodPluginOptions = {
    opsInterval: 1000,
    reporters: [{
        reporter: require('good-console'),
        events: { log: '*', response: '*' }
    }, {
        reporter: require('good-file'),
        events: { ops: '*' },
        config: './test/fixtures/awesome_log'
    }]
};

var pluginsArray = [
    {
        register: require('hapi-swagger'),
        options: swaggerOptions
    },
    {
        register : require('hapi-auth-jwt'),
        options : {}
    },
    {
        register: require('good'),
        options: goodPluginOptions
    }
];
exports.pluginsArray = pluginsArray;