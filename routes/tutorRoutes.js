/**
 * Created by Vikas Sharma on 7/8/15.
 */
'use strict';
var Joi = require('joi');
var Controller = require('../controllers');
var Constants = require('../utilities/constants');
var Util = require('../utilities/util')


/**
 * Route to view the favorite students of tutor
 */
var myFavorites = {
    method : 'GET',
    path : '/api/tutor/favorites/myFavorites',
    config :{
        description : 'View the favorite students of tutor',
        tags : ['api','tutor','favorites'],
        auth : 'token',
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.tutorController.myFavorites(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to view the earnings of tutor
 */
var myEarnings ={
    method : 'GET',
    path : '/api/tutor/myEarnings',
    config : {
        description : 'View the Earnings of tutor',
        tags : ['api','tutor','earnings'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.tutorController.myEarnings(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to view the current sessions of student
 */
var mySessions = {
    method : 'GET',
    path : '/api/tutor/mySessions',
    config : {
        description : 'View all the sessions of the tutor with given student/given course',
        tags : ['api','tutor','session'],
        validate : {
            query :{
                status : Joi.string().valid('ongoing','completed','scheduled'),
                studentId : Joi.string().length(24).hex(),
                courseId : Joi.string().length(24).hex()
            },
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query) {
                requestData.courseId = req.query.courseId;
                requestData.studentId = req.query.studentId;
                requestData.status = req.query.status;
            }
            Controller.tutorController.mySessions(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to view the courses of the tutor
 */
var myCourses = {
    method : 'GET',
    path : '/api/tutor/myCourses',
    config : {
        description : 'View the courses in which the tutor can teach',
        tags : ['api','tutor','courses'],
        validate :  {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction

        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.tutorController.myCourses(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Route to list the sessions of the tutor
 */
var myRequests = {
    method : 'GET',
    path : '/api/tutor/request/myRequests',
    config : {
        description : 'View the requests available to the user',
        tags : ['api','tutor','requests'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
                courseId : Joi.string().length(24).hex()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query.courseId) requestData.courseId = req.query.courseId;
            Controller.tutorController.myRequests(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 *  View the details of a particular request
 */
var viewRequest = {
    method  : 'GET',
    path : '/api/tutor/request/view',
    config : {
        description : 'View the details of a particular request',
        tags : ['api','tutor','requests'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
                requestId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId = req.query.requestId;
            Controller.tutorController.viewRequest(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Cancel a session
 */
var cancelSession = {
    method : 'DELETE',
    path : '/api/tutor/session/cancel',
    config : {
        description : 'Cancels a scheduled session',
        tags : ['api','tutor','session','cancel'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;;
            requestData.sessionId = req.query.sessionId;
            Controller.tutorController.cancelSession(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Reschedule a session
 */
var rescheduleSession = {
    method : 'PUT',
    path : '/api/tutor/session/reschedule',
    config : {
        description : 'Reschedules a session',
        tags : ['api','tutor','session','reschedule'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            payload : {
                startTime : Joi.date().min('now').required(),
                duration : Joi.number().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = req.payload;
            requestData.id = req.auth.credentials._id;
            requestData.sessionId = req.query.sessionId;
            Controller.tutorController.rescheduleSession(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Reject a request
 */
var rejectRequest = {
    method : 'PUT',
    path : '/api/tutor/request/reject',
    config : {
        description : 'Rejects a personal request sent to the tutor',
        tags : ['api','tutor','request','reject'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                requestId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId = req.query.requestId;
            Controller.tutorController.rejectRequest(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Accept a request
 */
var acceptRequest = {
    method : 'PUT',
    path : '/api/tutor/request/accept',
    config : {
        description : 'Accepts a personal request sent to the tutor',
        tags : ['api','tutor','request','accept'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                requestId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId = req.query.requestId;
            requestData.fee = req.query.fee;
            Controller.tutorController.acceptRequest(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
 /*
 * Modify a request
 */
var modifyRequest = {
        method : 'POST',
        path : '/api/tutor/request/modify',
        config : {
            description : 'Modifies a personal request sent to the tutor',
            tags : ['api','tutor','request','modify'],
            validate :{
                headers: Joi.object({
                    Authorization: Joi.string()
                }).options({allowUnknown: true}),
                query: {
                    requestId : Joi.string().length(24).hex().required()
                },
                payload : {
                    location : Joi.array().items(Joi.number()).length(2),
                    startTime : Joi.date().min('now'),
                    duration : Joi.number(),
                    notes : Joi.string().max(200)
                },
                failAction : Util.failActionFunction
            },
            auth : 'token',
            handler : function(req,reply) {
                var requestData = req.payload || {}
                requestData.id = req.auth.credentials._id;
                requestData.requestId = req.query.requestId;
                Controller.tutorController.modifyRequest(requestData, function (err, success) {
                    if (err) {
                        reply(err.response).code(err.statusCode);
                    }
                    else {
                        reply(success.response).code(success.statusCode);
                    }
                })
            }
        }
    }
/*
 * Send a request to student in reply to general request
 */
var sendRequest = {
    method : 'PUT',
    path : '/api/tutor/request/send',
    config : {
        description : 'Sends a request to student in response to general request posted by the user',
        tags : ['api','tutor','request','send'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                requestId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId = req.query.requestId;
            requestData.fee = req.params.fee;
            Controller.tutorController.sendRequest(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Send a request to the student to extend the session
 */
var extendSession = {
    method : 'PUT',
    path : '/api/tutor/session/extend',
    config : {
        description : 'Sends a request to student to extend the ongoing or scheduled sessions',
        tags : ['api','tutor','session','extend'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required(),
                extendTime : Joi.number().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.sessionId = req.query.sessionId;
            requestData.extendTime = req.query.extendTime;
            Controller.tutorController.extendSession(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * View the courses which are available to the user
 */
var availableCourses = {
    method : "GET",
    path : "/api/tutor/course/available",
    config : {
        description : 'View all the courses available to tutor',
        tags : ['api','tutor','course','available'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
              limit : Joi.number().integer().min(0),
              offset : Joi.number().integer().min(0)
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query){
                if(req.query.limit) requestData.limit = req.query.limit;
                if(req.query.offset) requestData.offset = req.query.offset;
            }
            Controller.tutorController.availableCourses(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Add a course to user's profile
 */
var addCourse = {
    method : "PUT",
    path : "/api/tutor/course/add",
    config : {
        description : 'Add a course to the tutor profile',
        tags : ['api','tutor','course','add'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
              courseId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.courseId = req.query.courseId;
            Controller.tutorController.addCourse(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Set my availability
 */
var setAvailability = {
    method : 'PUT',
    path : '/api/tutor/availability/set',
    config : {
        description : 'Set the availability of a tutor',
        tags : ['api','tutor','availability','set'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            payload: {
                availability : Joi.array().items(Joi.object({
                    dayId : Joi.number().integer().min(1).max(7).required(),
                    bit : Joi.array().items(Joi.number().valid(0,1)).length(24).required()
                })).length(7).allow(null).description('Send the array of availability with day_id and array of bits for which tutor is available')
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = req.payload || {} ;
            requestData.id = req.auth.credentials._id;
            Controller.tutorController.setAvailability(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * View my availability
 */
var viewAvailability = {
    method : 'GET',
    path : '/api/tutor/availability/view',
    config : {
        description : 'View my availability',
        tags : ['api','tutor','availability','view'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            //query: {
            //    dayId : Joi.number().integer().min(1).max(7).optional().description('1 is for sunday and so on')
            //},
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData =  {};
            requestData.id = req.auth.credentials._id;
            if(req.query && req.query.day) requestData.day = req.query.day;
            Controller.tutorController.viewAvailability(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Rate session
 */
var rateSession = {
    method : "PUT",
    path : '/api/tutor/rate',
    config : {
        description : 'Rate the student after a session',
        tags : ['api','tutor','rate','student'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            payload : {
                rating : Joi.array().items(Joi.number().min(1).max(5)).required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData =  req.payload || {};
            if(req.query && req.query.sessionId) requestData.sessionId = req.query.sessionId;
            requestData.id = req.auth.credentials._id;
            Controller.tutorController.rateStudent(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var tutorRoutes = [
    myFavorites,
    myEarnings,
    myCourses,
    mySessions,
    myRequests,
    viewRequest,
    cancelSession,
    rescheduleSession,
    rejectRequest,
    acceptRequest,
    modifyRequest,
    sendRequest,
    extendSession,
    availableCourses,
    addCourse,
    setAvailability,
    viewAvailability,
    rateSession];


module.exports = tutorRoutes;