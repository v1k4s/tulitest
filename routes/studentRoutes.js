/**
 * Created by Vikas Sharma on 7/8/15.
 */

var Joi = require('joi');
var Controller = require('../controllers');
var Constants = require('../utilities/constants');
var Util = require('../utilities/util');

/**
 * View my courses
 */
var viewTutors = {
    method : 'GET',
    path : '/api/student/tutor/view',
    config : {
        description : 'View the tutors who are available to teach a required course/(All courses in which student is enrolled)',
        tags : ['api','student','tutors','view'],
        validate :  {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
                courseId : Joi.string().length(24).hex(),
                limit : Joi.number().integer().min(0),
                offset : Joi.number().integer().min(0),
                tutorId : Joi.string().length(24).hex()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query){
                if(req.query.courseId) requestData.courseId = req.query.courseId;
                if(req.query.limit) requestData.limit = req.query.limit;
                if(req.query.offset) requestData.offset = req.query.offset;
                if(req.query.tutorId) requestData.tutorId = req.query.tutorId;
            }
            Controller.studentController.viewTutor(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * View my courses
 */
var myCourses = {
    method : 'GET',
    path : '/api/student/course/myCourses',
    config : {
        description : 'View the courses in which the student is enrolled',
        tags : ['api','student','courses'],
        validate :  {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.studentController.myCourses(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Route to to handle "Post New Request" by the student
 */
var newRequest = {
    method : 'POST',
    path : '/api/student/request/new',
    config : {
        description : 'Posts a new request general/personal',
        tags : ['api','student','request'],
        validate : {
            payload : {
                courseId : Joi.string().length(24).hex().required(),
                topics :  Joi.array().items(Joi.string()),
                location : Joi.string(),
                topics : Joi.array().items(Joi.string()),
                noOfStudents : Joi.number().integer().required(),
                tutorId : Joi.array().items(Joi.string().length(24).hex().allow(null)).optional(),
                requestType : Joi.string().valid('general','personal'),
                requestTime : Joi.string().valid('onDemand','scheduled'),
                startTime : Joi.date().min('now'),
                duration : Joi.number().max(Constants.MAX_DURATION).required(),
                feeLower : Joi.number().min(Constants.prices.MIN_PRICE),
                feeUpper : Joi.number().max(Constants.prices.MAX_PRICE),
                description : Joi.string(),
                sessionType : Joi.string().valid(Constants.SESSION_TYPES)
            },
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            if(req.payload) requestData= req.payload;
            requestData.id = req.auth.credentials._id;
            Controller.studentController.newRequest(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to to view requests posted by user
 */
var myRequests = {
    "method" : "GET",
    "path" : "/api/student/myRequests",
    "config" : {
        description : 'View the current requests posted by the student',
        tags : ['api','student','request'],
        validate :  {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = req.payload || {};
            requestData.id = req.auth.credentials._id;
            Controller.studentController.myRequests(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to view the current sessions of student
 */
var mySessions = {
    method : 'GET',
    path : '/api/student/mySessions',
    config : {
        description : 'View all the sessions of the student with given tutor/given course',
        tags : ['api','student','session'],
        validate : {
            query :{
                status : Joi.string().valid('ongoing','completed','scheduled'),
                tutorId : Joi.string().length(24).hex(),
                courseId : Joi.string().length(24).hex()
            },
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query) {
                requestData.courseId = req.query.courseId;
                requestData.tutorId = req.query.tutorId;
                requestData.status = req.query.status;
            }
            Controller.studentController.mySessions(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to handle th e tutors of student
 */
var myTutors = {
    method : 'GET',
    path : '/api/student/tutor/myTutors',
    config : {
        description : 'View the tutors who have taught/scheduled to taught/currently teaching',
        tags : ['api','student','tutors'],
        auth : 'token',
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.studentController.myTutors(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to show favorite tutors
 */
var myFavorites = {
    method : 'GET',
    path : '/api/student/favorites',
    config :{
        description : 'View the favorite tutors of student',
        tags : ['api','student','favorites'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.studentController.myFavorites(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Route to show payments made
 */
var myPayments ={
    method : 'GET',
    path : '/api/student/myPayments',
    config : {
        description : 'View the payments made by the student',
        tags : ['api','student','payments'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            Controller.studentController.myPayments(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


/**
 * Route to view the tutor

 var viewTutor = {
    method : 'GET',
    path : '/api/student/tutors/viewTutor',
    config : {
        description : 'View the details of a particular user',
        tags : ['api','student','tutors'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
                courseId : Joi.string().length(24),
                tutorId : Joi.string().length(24),
            }
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if (req.query.courseId)  requestData.courseId = req.query.courseId;
            if(req.query.tutorId) requestData.tutorId = req.query.tutorId;
            Controller.studentController.viewTutor(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
} */                        // Depreciated


/**
 * Route to mark a tutor as favorite
 */
var markFavorite = {
    method : 'PUT',
    path : '/api/student/favorites',
    config : {
        description : 'Mark a tutor as a favorite tutor',
        tags : ['api','student','favorite'],
        validate : {
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            query : {
                tutorId : Joi.string().length(24).required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.tutorId = req.query.tutorId;
            Controller.studentController.markFavorite(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Route to view available route corresponding to class and year of user
 */
var availableCourses = {
    method : 'GET',
    path : '/api/student/course/available',
    config : {
        description : 'View available courses for the user',
        tags : ['api','student','courses'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query : {
                limit : Joi.number().integer().min(0),
                offset : Joi.number().integer().min(0)
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query){
                if(req.query.limit) requestData.limit = req.query.limit;
                if(req.query.offset) requestData.offset = req.query.offset;
            }
            Controller.studentController.availableCourses(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Route to add a course student is enrolled in
 */
var addCourse = {
    method :'PUT',
    path : '/api/student/course/add',
    config : {
        description : 'Add a course in which the student is enrolled in',
        tags : ['api','student','courses'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            payload: {
                courseId : Joi.array().items(Joi.string().length(24).hex()).min(1).max(6).required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = req.payload || {};
            requestData.id = req.auth.credentials._id;;
            Controller.studentController.addCourse(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Route to cancel a scheduled session
 */
var cancelSession = {
    method : 'DELETE',
    path : '/api/student/session/cancel',
    config : {
        description : 'Cancels a scheduled session',
        tags : ['api','student','session','cancel'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;;
            requestData.sessionId = req.query.sessionId;
            Controller.studentController.cancelSession(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Reschedule a session
 */
var rescheduleSession = {
    method : 'POST',
    path : '/api/student/session/reschedule',
    config : {
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            payload : {
                startTime : Joi.date().min('now').required(),
                duration : Joi.number().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.sessionId = req.query.sessionId;
            Controller.studentController.res(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * Extend an ongoing session
 */
var extendSession = {
    method : 'PUT',
    path : '/api/student/session/extend',
    config : {
        description : 'Extends an ongoing session',
        tags : ['api','student','session','extend'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {}
            requestData.id = req.auth.credentials._id;
            requestData.sessionId = req.query.sessionId;
            requestData.extendTime = req.query.extendTime;
            Controller.studentController.extendSession(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * View the availability of a tutor
 */
var viewAvailability = {
    method : "GET",
    path : '/api/student/availability/view',
    config : {
        description : 'View tha availability of a tutor',
        tags : ['api','student','tutor','availability'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                tutorId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.tutorId = req.query.tutorId;
            Controller.studentController.viewAvailability(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Rate session
 */
var rateSession = {
    method : "PUT",
    path : '/api/student/rate',
    config : {
        description : 'Rate the tutor after a session',
        tags : ['api','student','rate','tutor'],
        validate :{
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                sessionId : Joi.string().length(24).hex().required()
            },
            payload : {
                rating : Joi.array().items(Joi.number().min(1).max(5)).required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData =  req.payload || {};
            if(req.query && req.query.sessionId) requestData.sessionId = req.query.sessionId;
            requestData.id = req.auth.credentials._id;
            Controller.studentController.rateStudent(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}
/**
 * View the details of a particular request
 */
var viewRequest = {
    method : "GET",
    path : '/api/student/request/view',
    config : {
        description : 'View tha details of a particular request',
        tags : ['api','student','request','view'],
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                requestId : Joi.string().length(24).hex().required()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId  = req.query.requestId;
            Controller.studentController.viewRequest(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })

        }
    }
}
/**
 * Accept the request from tutor and schedule a session
 */
var acceptRequest = {
    method : 'PUT',
    path : "/api/student/request/accept",
    config : {
        description: 'Accept a particular request and schedule a session',
        tags: ['api', 'student', 'request', 'accept'],
        validate: {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            query: {
                requestId: Joi.string().length(24).hex().required(),
                tutorId: Joi.string().length(24).hex()
            },
            failAction: Util.failActionFunction
        },
        auth: 'token',
        handler: function (req, reply) {
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            requestData.requestId = req.query.requestId;
            if(req.query.tutorId) requestData.tutorId = req.query.tutorId;
            Controller.studentController.acceptRequest(requestData, function (err, success) {
                if (err) {
                    reply(err.response).code(err.statusCode);
                }
                else {
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var studentRoutes = [
    newRequest,
    myRequests,
    mySessions,
    myTutors,
    myFavorites,
    myPayments,
    myCourses,
    markFavorite,
    availableCourses,
    addCourse,
    cancelSession,
    rescheduleSession,
    extendSession,
    viewAvailability,
    rateSession,
    viewTutors,
    viewRequest,
    acceptRequest
];
module.exports = studentRoutes;