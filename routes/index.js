/**
 * Created by Vikas Sharma on 7/8/15.
 */
var adminRoutes = require('./adminRoutes');
var userRoutes = require('./userRoutes');
var studentRoutes = require('./studentRoutes');
var tutorRoutes = require('./tutorRoutes');

exports.routes = [adminRoutes, userRoutes,studentRoutes,tutorRoutes];