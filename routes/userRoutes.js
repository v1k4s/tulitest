/**
 * Created by Vikas Sharma on 7/8/15.
 */

var Joi = require('joi');
Joi.phone = require('joi-phone');
var Controller = require('../controllers');
var Constants = require('../utilities/constants');
var Util = require('../utilities/util');


var login = {
    method : 'POST',
    path : '/api/user/login',
    config : {
        description : 'Main Login Path for both student, tutor and admin',
        tags : ['api','user','login'],
        validate : {
            payload : {
                email : Joi.when('fbLoginFlag',{is : 0, then : Joi.string().email().required()}).description('Enter either Email registered with your account. Not required in case of Fb login'),
                password : Joi.when('fbLoginFlag',{is : 0, then : Joi.string().required()}),
                fbLoginFlag : Joi.number().valid(1,0).required().description("whether login through facebook or not"),
                fbId : Joi.when('fbLoginFlag', { is: 1, then: Joi.string().invalid('').required() }).description('Required in case of a Facebook register'),
                deviceType : Joi.string().valid('ios','android').insensitive(),
                appVersion : Joi.string().regex(/^(\d+\.)?(\d+\.)?(\*|\d+)$/).description('For updation of app in future'),
                deviceToken : Joi.string().description('For sending push notifications')
            },
            failAction : Util.failActionFunction
        },
        handler : function(req,reply) {
            var userData = req.payload;
            Controller.userController.login(userData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}



var signUp = {
    "method": "POST",
    "path": "/api/user/signup",
    "config": {
        description: 'Registration page of new user',
        tags: ['api', 'user', 'signup'],
        "payload" : {
            allow : 'multipart/form-data',
            parse :  true,
            output : "stream"
        },
        validate: {
            payload: {
                role: Joi.string().valid('student','tutor','both').insensitive().required(),
                firstName: Joi.string().regex(/^[a-zA-Z]{3,20}$/).required(),
                lastName: Joi.string().regex(/^[a-zA-Z]{3,20}$/).invalid(Joi.ref('firstName')).required(),
                fbRegisterFlag : Joi.number().valid(1,0).required().description('Send true for facebook register'),
                ///userName: Joi.string().regex(/^[a-z0-9_-]{3,16}$/).required(),
                ////    ((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20}) Regex for password
                password : Joi.when('fbRegisterFlag', { is: 0, then: Joi.string().required() }).description('Not required in case of a Facebook register'),
                fbId : Joi.when('fbRegisterFlag', { is: 1, then: Joi.string().invalid('').required() }).description('Required in case of a Facebook register'),
                profilePicture: Joi.object().meta({swaggerType: 'file'}).optional(),
                college: Joi.string().required(),
                year: Joi.number().integer().required(),
                primaryEmail : Joi.string().email({tldWhitelist : Constants.emailWhiteList}).required().description('Only Edu emails accepted'),
                facebookEmail : Joi.string().email().allow('').description('Email pulled from the facebook profile'),
                mobileNumber : Joi.string().required().description('Phone number in any format'),
                majors: Joi.string().optional().allow(''),
                minors : Joi.string().optional().allow(''),
                bioDescription: Joi.string().allow(''),
                transcript: Joi.array().items(Joi.object().meta({ swaggerType: 'file' })).allow(null).description('Array of the transcript files of the tutor'),
                //availability : Joi.object({
                //    monday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional().description('Array items correspond to the hours of the day in which the user is available. For example if you send [2,3], it means tutor is available from 2 to 3pm, send [14]'),
                //    tuesday: Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional(),
                //    wednesday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional(),
                //    thursday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional(),
                //    friday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional(),
                //    saturday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional(),
                //    sunday : Joi.array().items(Joi.number().min(0).max(23)).max(24).unique().optional()
                //}).optional().allow(null),
                availability : Joi.array().items(Joi.object({
                    dayId : Joi.number().integer().min(1).max(7).required(),
                    bit : Joi.array().items(Joi.number().valid(0,1)).length(24).required()
                })).allow(null).description('Send the array of availability with day_id and array of bits for which tutor is available'),
                courses : Joi.array().items(Joi.object({
                   courseId :  Joi.string().hex().required(),
                   courseFee : Joi.number().min(0).optional()
                })).unique().optional().description('Send a object of courseIds and corresponding fees'),
                stripeToken : Joi.string().optional().allow('')
            },
            failAction : Util.failActionFunction
        },
            handler : function(req,reply){
                var requestData = req.payload;
                requestData.roles = [];
                if(requestData.role.toLowerCase() != 'both'){
                    requestData.roles.push(requestData.role.toLowerCase());
                } else if(requestData.role.toLowerCase() == 'both'){
                    requestData.roles.push('student');
                    requestData.roles.push('tutor');
                } else {
                    var response = {
                        message : "Invalid Role",
                        data : {}
                    }
                    reply(response).code(400);
                }
                delete requestData.role;
                Controller.userController.signUp(requestData, function(err, success){
                    if(err){
                        reply(err.response).code(err.statusCode);
                    }
                    else{
                        reply(success.response).code(success.statusCode);
                    }
                })
            }

    }
}


var verify = {
    "method" : "GET",
    "path" : "/api/user/verify",
    "config" : {
        description: 'Verifies the user from the verification link sent in the email',
        tags: ['api', 'user', 'verify'],
        validate : {
            query:{
                token : Joi.string()
            },
            failAction : Util.failActionFunction
        },
        handler : function(req,reply){
            var requestData = {};
            requestData.tokenData = req.token;
            Controller.userController.verify(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var logOut = {
    method : 'PUT',
    path : '/api/user/logout',
    config : {
        description : 'Logout a user and end his/her session',
        tags : ['api','logout','student','tutor'],
        auth : 'token',
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            failAction : Util.failActionFunction
        },
        handler : function(req,reply){
            var requestData = {};
            var request = req.raw.req;
            var Authorization = request.headers.authorization;
            console.log(Authorization);
            var parts = Authorization.split(/\s+/);
            requestData.token = parts[1];
            Controller.userController.logOut(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var myProfile = {
    method : 'GET',
    path : '/api/user/profile/view',
    config : {
        description : 'View my Profile',
        tags : ['api','user','profile'],
        auth : 'token',
        validate : {
            headers: Joi.object({
                Authorization: Joi.string()
            }).options({allowUnknown: true}),
            failAction : Util.failActionFunction
        },
        handler : function(req,reply) {
            var userData = req.payload || {};
            userData.id = req.auth.credentials._id;
            Controller.userController.myProfile(userData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var updateProfile = {
    method : 'POST',
    path : '/api/user/profile/update',
    config : {
            description : "Updates the profile of a user",
            tags : ['api','user','profile','update'],
            payload : {
            allow : 'multipart/form-data',
            parse :  true,
            output : "stream"
             },
            validate : {
                    headers: Joi.object({
                        Authorization: Joi.string()
                    }).options({allowUnknown: true}),
                    payload: {
                        firstName: Joi.string().regex(/^[a-zA-Z]{3,20}$/).allow(null),
                        lastName: Joi.string().regex(/^[a-zA-Z]{3,20}$/).invalid(Joi.ref('firstName')).allow(null),
                        profilePicture: Joi.object().meta({swaggerType: 'file'}).optional().allow(null),
                        college: Joi.string().allow(null),
                        bioDescription: Joi.string().max(200).allow(null),
                        password : Joi.string()
                        // More things to update
                    },
                    failAction : Util.failActionFunction
                },
            auth : 'token',
            handler : function(req,reply){
                var requestData = req.payload;
                requestData.id = req.auth.credentials._id;
                Controller.userController.updateProfile(requestData,function(err, success){
                    if(err){
                        reply(err.response).code(err.statusCode);
                    }
                    else{
                        reply(success.response).code(success.statusCode);
                    }
                })
            }
    }
}


var availableCourses = {
    method : 'GET',
    path : '/api/user/courses/available',
    config : {
        description : 'Available courses for user at the time of login',
        tags : ['api','user','login'],
        handler : function(req,reply) {
            Controller.userController.availableCourses(function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var loginRoutes = [login, signUp,verify,logOut,myProfile,updateProfile,availableCourses];


module.exports = loginRoutes;