/**
 * Created by Vikas Sharma on 7/8/15.
 */

var Joi = require('joi');
var Controller = require('../controllers');
var Util = require('../utilities/util');


var showSessions = {
    method : 'GET',
    path : '/api/admin/sessions',
    config : {
        description : 'Shows all the sessions to the admin',
        tags : ['api','admin','sessions'],
        validate : {
                query:{
                    sessionId : Joi.string().length(24).hex(),
                    status : Joi.any().valid('completed','ongoing','scheduled')
                },
                headers : Joi.object({
                                Authorization : Joi.string()
                                }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: [
                    {code: 200, message: 'Request OK'},
                    {code: 400, message: 'Bad Request'},
                    {code: 500, message: 'Internal Server Error'}
                ]
            }
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {} ;
                requestData.id = req.auth.credentials._id;
            if(req.query && req.query.status) requestData.sessionStatus = req.query.status;
            if(req.query && req.query.sessionId) requestData.sessionId = req.query.sessionId;
            Controller.adminController.showSessions(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var showUser = {
    "method" : 'GET',
    "path" : '/api/admin/user/view',
    "config" : {
        description : 'Shows the details of a particular user/All users',
        tags : ['api','admin','user'],
        validate :{
            query :{
                userId : Joi.string().length(24).hex(),
                role : Joi.string().valid('student','tutor').optional().allow(null),
                limit : Joi.number().integer().min(1),
                offset : Joi.number().integer()
            },
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = {};
            requestData.id = req.auth.credentials._id;
            if(req.query){
                if(req.query.userId) requestData.userId = req.query.userId;
                if(req.query.limit) requestData.limit = req.query.limit;
                if(req.query.offset) requestData.offset = req.query.offset;
                if(req.query.role) requestData.role = req.query.role;
            }
            Controller.adminController.showUser(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var approveTutor = {
     "method" : 'PUT',
     "path" : '/api/admin/approveTutor',
     "config" : {
         description : 'Approves a particular tutor',
         tags : ['api','admin','tutor','approve'],
         validate :{
             query :{
                 tutorId : Joi.string().length(24).hex().required()
             },
             headers : Joi.object({
                 Authorization : Joi.string()
             }).options({ allowUnknown: true }),
             failAction : Util.failActionFunction
         },
         auth : 'token',
         handler : function(req,reply){
             var requestData = {};
             requestData.id = req.auth.credentials._id;
             requestData.tutorId = req.query.tutorId;
             Controller.adminController.approveTutor(requestData, function(err, success){
                 if(err){
                     reply(err.response).code(err.statusCode);
                 }
                 else{
                     reply(success.response).code(success.statusCode);
                 }
             })
         }
     }
}


var addCourse = {
    method : 'POST',
    path : '/api/admin/course/add',
    config : {
        description : 'Add a particular course',
        tags : ['api','admin','course','add'],
        validate :{
            headers : Joi.object({
                Authorization : Joi.string()
            }).options({ allowUnknown: true }),
            payload :{
                courseCode : Joi.string().regex(/^[A-Z0-9-]{3,8}$/).required().description('The course code should be of the format MK-102'),
                courseTitle : Joi.string().regex(/^[A-Za-z0-9 ]{5,50}$/).max(50).required(),
                topics : Joi.array().items(Joi.string()).optional().allow(null),
                desc : Joi.string().max(100).required(),
                class : Joi.string().max(20).optional()
            },
            failAction : Util.failActionFunction
        },
        auth : 'token',
        handler : function(req,reply){
            var requestData = req.payload || {};
            requestData.id = req.auth.credentials._id;
            Controller.adminController.addCourse(requestData, function(err, success){
                if(err){
                    reply(err.response).code(err.statusCode);
                }
                else{
                    reply(success.response).code(success.statusCode);
                }
            })
        }
    }
}


var adminRoutes = [showSessions,showUser,approveTutor,addCourse]


module.exports = adminRoutes;