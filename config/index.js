/**
 * Created by Vikas Sharma on 7/8/15.
 */

module.exports = {
    db: {
        development: "mongodb://ghjg@ghgj:localhost:27017/tuli_development",
        test: 'mongodb://localhost:27017/tuli_test'
    },
    mailer: {
        auth: {
            user: 'asdfa@gmail.com',
            pass: 'sadfa'
        },
        service: 'Gmail'
    },
    server: {
        host: '0.0.0.0',
        port: {
            dev: 5426,
            test: 6254
        }
    },
    secret: {
        key: '^+3BaD?CE4atc&='
    },
    admin: {
        roles: ['admin'],
        primaryEmail: "admin@admin.edu",
        password: "qwe123!@#",
        firstName: "admin",
        lastName: "admin",
        mobileNumber: "9999999999",
        isVerified: true,
        profilePicture: "sdfadas",
        profilePictureThumb: "hsj",
        year: "2015",
        fbRegisterFlag: 0
    },
    timeZone: {
        adjust: 0.0
    },
    s3BucketCredentials: {
        bucket: "tuliapp",
        accessKeyId: "AKIAJL2AZXM5ZKDZMOGA",
        secretAccessKey: "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",
        s3URL: "http://tuliapp.s3.amazonaws.com",
        defaultPicUrl: "http://tuliapp.s3.amazonaws.com/default/default_profile_pic.jpg",
        folder: {
            original: "original",
            tutor: "tutor",
            profilePicture: "profilePicture",
            thumb: "thumbNail",
            transcript: "transcript"
        }

    },
    stripe: {
        testKey: 'djskfkasdf',
        liveKey: 'dsadsadasdsadas'
    }
};
