/**
 * Created by Vikas Sharma on 7/2/15.
 */

var Constants = require('../utilities/constants');
var Model   = require('../models');
var Dao = require('../dao/DAO');
var Async = require('async');
var Util = require('../utilities/util');
var TokenFunctions = require('../utilities/tokenFunctions');

/**
 * Check if a field exists in the database with a particular _id
 */
exports.fieldValidation = function(model, id, projection, options, errorMessage, callbackController ){
    Async.waterfall([
        function(callback){                                                         // 1st function to frame the data and get
            var condition = {
                _id: id
            };
            Dao.getData(model, condition, projection, options, callback);           // Call the database function
        },
        function(data, callback){                                                   // 2nd function to frame error responses
            if(data.length ==0 || data == null){
                var response = {
                    message: errorMessage,                                          // Not Found Error response
                    data: {}
                };
                var errorResponse = {
                    response: response,
                    statusCode: 404
                };
                callback(errorResponse);
            }
            else{
                callback(null, data);                                               // No error and call the next function by passing data
            }
        }
    ],function(err, result){                                                        // last function to catch errors or send response in case of success
        if(err){
            callbackController(err);
        }
        else{
            callbackController(null, result);
        }
    })
}

/**
 * Check if a user is a admin/student/tutor
 */

exports.roleValidation = function(id,role,responseMessage,callbackController){
    var projection = {};
    var options = {lean:true , limit : 1};
    Async.waterfall([
        function(callback){
            var condition = {
                _id : id,
                roles : role
            }
            Dao.getData(Model.User,condition,projection,options,callback);
        },
        function(data,callback) {
            if (data.length == 0 || data == null) {
                var response = {
                    message : responseMessage,
                    data : {}
                }
                var errorResponse = {
                    response : response,
                    statusCode : 403
                }
                callback(errorResponse);
            } else if ((data[0].roles.indexOf('tutor') != -1) && data[0].isApprovedTutor == false){
                var response = {
                    message : Constants.responseMessage.TUTOR_NOT_APPROVED,
                    data : {}
                }
                var errorResponse = {
                    response : response,
                    statusCode : 403
                }
                callback(errorResponse);
            } else {
                callback(null,data);
            }
        }
    ],
    function(err,result){
        if(err){
            callbackController(err);
        } else {
            callbackController(null,result);
        }
    })
}

/*
* Validate a token
*/
exports.tokenValidation = function(decodedToken,callbackAuth){
    Async.waterfall([
            function (callback) {
             TokenFunctions.verifyToken(decodedToken,callback);
            },
            function(data,callback){
            Dao.getData(Model.User,{_id :decodedToken.id},{firstName : 1, lastName : 1},{lean: true, limit:1}, callback);
            }
    ],
    function(err,result){
        if(err) callbackAuth(null,false,result);
        else if (result == null || result.length == 0) callbackAuth(null,false,result[0]);
        else callbackAuth(null,true,result[0]);
    })
}