/**
 * Created by harekam on 4/23/15.
 * Edited by Vikas on 7/2/2015
 */

var request = require('request');
var AWS = require('aws-sdk')
var constants = require('./constants');
var DAO = require('../dao/DAO');
var momentTimezone = require('moment-timezone');
var Distance = require('geo-distance');
var config = require('../config/index');
var dist = require('geo-distance-js');
var async = require('async');
var models = require('../models');
var gcm = require('node-gcm');
var debugging_enabled = true;
var apns = require('apn');
var nodemailer = require('nodemailer');
var moment = require('moment');
var Crypto = require('crypto');
var fs = require('fs');
var Logger = require('./logger')
//var fs= require('node-fs');
//var aws = require('aws-sdk');
/*
 * -----------------------------------------------
 * CHECK EACH ELEMENT OF ARRAY FOR BLANK.
 * -----------------------------------------------
 */
function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === undefined) {
            arr[i] = "";
        } else {
            arr[i] = arr[i];
        }
        arr[i] = arr[i];
        if (arr[i] === '' || arr[i] === "" || arr[i] == undefined) {
            return 1;
        }
    }
    return 0;
}

/*
 * -----------------------------------------------
 * CHECK BLANK WITH CALL BACK.
 * -----------------------------------------------
 */

exports.checkBlankWithCallback = function (blankData, callback) {
    var checkBlankData = checkBlank(blankData);
    if (checkBlankData) {

        var errResponse = {
            message: constants.responseMessage.PARAMETER_MISSING,
            data: {}
        };
        callback(errResponse);
    } else {
        callback(null);
    }
};


//TODO evaluate logic behind this
exports.createJsonFromPayload = function (payload) {
    var data = {};
    for (var key in payload) {
        if (payload.hasOwnProperty(key) && payload[key]) {
            data[key] = payload[key];
        }
    }
    return data;
};

/*
 return date in UTC format Eg:(2015-04-29 06:24:40) from local (Wed Apr 29 2015 11:54:40 GMT+0530 (IST))
 */
exports.getUTCDate = function () {
    return new Date().toISOString();
};


exports.verifyAuthorization = function (id, user, accessToken, callbackResult) {
    async.waterfall([
        function (callback) {

            var condition = {_id: id};
            var projection = {accessToken: 1};
            var options = {limit: 1, lean: true};

            if (user === constants.types.customer) {

                DAO.getData(models.customer, condition, projection, options, callback);

            } else if (user === constants.types.serviceProvider) {

                DAO.getData(models.serviceProviders, condition, projection, options, callback);

            }
        }, function (data, callback) {
            var error;
            if (data && data.length > 0) {
                if (accessToken == data[0].accessToken) {
                    callback(null);
                } else {
                    error = {
                        response: {
                            message: constants.responseMessage.INVALID_ACCESS,
                            data: {}
                        },
                        statusCode: 401
                    };
                    callback(error);
                }
            } else {
                error = {
                    response: {
                        message: constants.responseMessage.USER_NOT_FOUND,
                        data: {}
                    },
                    statusCode: 404
                };
                callback(error);
            }
        }
    ], function (error) {
        if (error) {
            callbackResult(error);
        } else {
            callbackResult(null);
        }
    })
};

function slotsToMinutes(slots) {
    var bits = slotsToBits(slots);
    var minutes = [], count = 0;
    for (var i = 0; i < 48; i++) {
        if (bits[i] == 1) {
            minutes.push(count);
        }
        count += 30;
    }
    return minutes;
}

function slotsToBits(slots) {

    var count = 0, counter = 0.5, k = 0, bits = [];
    var slotsLen = slots.length;
    for (var i = 0; i < 48; i++) {
        if (k < slotsLen) {
            if (count >= slots[k] && count < slots[k + 1]) {
                bits[i] = 1;
            } else if (count == slots[k + 1]) {
                bits[i] = 1;
                k += 2;
            }
        } else {
            bits[i] = 0;
        }
        count += counter;

    }
    return bits;
}
exports.processSlots = function (data) {
    var availability = {};
    availability.Monday = slotsToMinutes(data.Monday);
    availability.Tuesday = slotsToMinutes(data.Tuesday);
    availability.Wednesday = slotsToMinutes(data.Wednesday);
    availability.Thursday = slotsToMinutes(data.Thursday);
    availability.Friday = slotsToMinutes(data.Friday);
    availability.Saturday = slotsToMinutes(data.Saturday);
    availability.Sunday = slotsToMinutes(data.Sunday);
    return availability;
};

function getLocalAndUtcTime(datetime, timezone) {

    // var formatReadable = "dddd, MMMM Do YYYY, HH:mm:ss";
    var format = 'YYYY/MM/DD HH:mm';
    var year = datetime.getFullYear();
    var month = datetime.getMonth();
    var day = datetime.getDate();
    var hour = datetime.getHours();
    var minute = datetime.getMinutes();
    var totalMinutes = hour * 60 + minute;
    var second = datetime.getMinutes();
    var millisecond = datetime.getMilliseconds();
    var Numbers = [year, month, day, hour, minute, second, millisecond];
    var dateTimeLocal = momentTimezone.tz(Numbers, timezone);
    var dateTimeAll = {
        dateTimeLocal: dateTimeLocal.format(format),
        dateTimeUTC: dateTimeLocal.utc().format(format),
        dateTimeUTCinDate: dateTimeLocal.toISOString(),
        day: dateTimeLocal.format('dddd'),
        totalMinutes: totalMinutes

    };
    console.log("time", dateTimeAll);
    return dateTimeAll;

}
function formatDateTime(datetime, format) {
    if (!format) {
        format = 'YYYY/MM/DD HH:mm';
    }
    var year = datetime.getFullYear();
    var month = datetime.getMonth();
    var day = datetime.getDate();
    var hour = datetime.getHours();
    var minute = datetime.getMinutes();
    var second = datetime.getMinutes();
    var millisecond = datetime.getMilliseconds();
    var Numbers = [year, month, day, hour, minute, second, millisecond];
    var momentDateTime = moment(Numbers);
    var formattedDateTime = momentDateTime.format(format);
    return formattedDateTime;
}

/*
 @from = {lat: lat1, lng: long1}
 @to = [{lat: lat2, lng: long2},{lat: lat2, lng: long2}]
 */

function calculateDistance(from, to) {

    //var from = {lat: lat1, lng: long1};
    //var to = [{lat: lat2, lng: long2}];

    var result = dist.getDistance(from, to, 'asc', 'metres', 2);
    return result;
}
function distanceBetween(from, geoDetails) {

    var len = geoDetails.length;
    for (var i = 0; i < len; i++) {

        var distanceDetails = Distance.between(from, geoDetails[i].to);
        distanceDetails = distanceDetails.human_readable();
        geoDetails[i].distance = parseInt(distanceDetails.distance);
    }
    return geoDetails;
}

function contains(array, arrayToBeChecked) {

    sortArray(array, 'asc');
    sortArray(arrayToBeChecked, 'asc');

    var lenCheck = arrayToBeChecked.length;
    var len = array.length;
    var i = 0, j = 0;
    while (i < len && j < lenCheck) {
        if (array[i] == arrayToBeChecked[j]) {
            j++;
        }
        i++;
    }
    return (j >= lenCheck);

}
function sortArray(array, order) {

    if (order.toLowerCase() == 'asc') {

        array.sort(function (a, b) {
            return a - b
        });

    } else if (order.toLowerCase() == 'dsc') {

        array.sort(function (a, b) {
            return b - a
        });

    }
}


/*
 //GENERIC SORTING
 */
(function () {
    if (typeof Object.defineProperty === 'function') {
        try {
            Object.defineProperty(Array.prototype, 'sortBy', {value: sb});
        } catch (e) {
        }
    }
    if (!Array.prototype.sortBy) Array.prototype.sortBy = sb;

    function sb(f) {
        for (var i = this.length; i;) {
            var o = this[--i];
            this[i] = [].concat(f.call(o, o, i), o);
        }
        this.sort(function (a, b) {
            for (var i = 0, len = a.length; i < len; ++i) {
                if (a[i] != b[i]) return a[i] < b[i] ? -1 : 1;
            }
            return 0;
        });
        for (var i = this.length; i;) {
            this[--i] = this[i][this[i].length - 1];
        }
        return this;
    }
})();


/*
 ==========================================================
 Send the notification to the iOS device for customer
 ==========================================================
 */
exports.sendIosPushNotification = function (iosDeviceToken, message, payload) {

    console.log(payload);

    console.log(config.iOSPushSettings.iosApnCertificate);
    console.log(config.iOSPushSettings.gateway);

    if (payload.address) {
        payload.address = '';
    }
    var status = 1;
    var msg = message;
    var snd = 'ping.aiff';
    //if (flag == 4 || flag == 6) {
    //    status = 0;
    //    msg = '';
    //    snd = '';
    //}


    var options = {
        cert: config.iOSPushSettings.iosApnCertificate,
        certData: null,
        key: config.iOSPushSettings.iosApnCertificate,
        keyData: null,
        passphrase: 'CPS',
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: config.iOSPushSettings.gateway,
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true
    };


    var deviceToken = new apns.Device(iosDeviceToken);
    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.contentAvailable = 1;
    note.sound = snd;
    note.alert = msg;
    note.newsstandAvailable = status;
    note.payload = {message: payload};

    apnsConnection.pushNotification(note, deviceToken);

    // Handle these events to confirm that the notification gets
    // transmitted to the APN server or find error if any
    function log(type) {
        return function () {
            if (debugging_enabled)
                console.log("iOS PUSH NOTIFICATION RESULT: " + type);
        }
    }

    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));

};


/*
 ==============================================
 Send the notification to the android devices
 =============================================
 */
exports.sendAndroidPushNotification = function (registrationIds, message) {
    Logger.debug(registrationIds,"Registration Ids of Android devices");
    Logger.debug(message,"Message to send");
    var message = new gcm.Message({
        collapseKey: 'demo',
        delayWhileIdle: false,
        timeToLive: 2419200,
        data: {
            message: message,
            brand_name: config.androidPushSettings.brandName
        }
    });
    var sender = new gcm.Sender(config.androidPushSettings.gcmSender);
    if (registrationIds.length == 0) {
        return 0;
    } else
    {
        sender.send(message, registrationIds, 4, function (err, result) {
            if (debugging_enabled) {
                Logger.info("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
                Logger.info("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
            }
        });
        return 1;
    }
};


/*
 ==============================================
 send mail with defined transport object
 =============================================
 */
exports.sendEmail = function (mailOptions, callback) {

    // create reusable transporter object using SMTP transport
    var transporter = nodemailer.createTransport({
        service: config.mailer.service,
        auth: {
            user: config.mailer.auth.user,
            pass: config.mailer.auth.pass
        }
    });

    // NB! No need to recreate the transporter object. You can use
// the same transporter object for all e-mails

// setup e-mail data with unicode symbols
//var mailOptions = {
//   from: 'Serv2day <invite@serv2day.com>', // sender address
//    to: 'harekamsingh@gmail.com', // list of receivers
//    subject: 'Invitation for serv2day', // Subject line
//    text: 'Hello world ✔', // plaintext body
//    html: '<b>Hello world ✔</b>' // html body
//};
transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            var response = {
                message: constants.responseMessage.FAILURE,
                data: error
            };
            var errorResponse = {
                response: response,
                statusCode: 401
            };
            callback(errorResponse)
        }
        else{
            callback(null, info)
        }
        console.log('Message sent: ' + info);

    });
};


/*
 ========================================
 Extras

 ========================================
 */


exports.convertToLocalTimezone = function (data, timezone) {

    var len = data.length;

    for (var i = 0; i < len; i++) {
        var datetime = getLocalAndUtcTime(data[i].startTime, timezone);
        data[i].startTime = datetime.dateTimeLocal;
        console.log(">>>>>>", datetime.dateTimeLocal, "<<<<<<<");
    }

    return data;
};

exports.timeDifferenceInDays = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 86400000);
};


exports.generateRandomString = function () {
    var text = "";
    var possible = "0123456789abcdefghijklmnopqrstuvwxyz";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

exports.setCharAt = function (str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
};


exports.encrypt = function (text) {
    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};


exports.sortByKeyAsc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};
function sortAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}


// Format the raw address obtained using google API
exports.formatLocationAddress = function (raw_address) {
    var pickup_location_address = 'Unnamed';

    var arr_formatted_address = raw_address.split(',');

    var arr_formatted_address_length = arr_formatted_address.length;
    var arr_pickup_location_address = [];
    for (var i = 0; i < arr_formatted_address_length; i++) {
        var flag = 0;
        for (var j = 0; j < arr_formatted_address_length; j++) {
            if ((i != j) && (arr_formatted_address[j].indexOf(arr_formatted_address[i]) > -1)) {
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            arr_pickup_location_address.push(arr_formatted_address[i]);
        }
    }

    pickup_location_address = arr_pickup_location_address.toString();
    return pickup_location_address;
};


// Get the address of the location using the location's latitude and longitude
exports.getLocationAddress = function (latitude, longitude, callback) {
    request('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude, function (error, response, body) {
        var pickup_address = 'Unnamed';
        if (!error && response.statusCode == 200) {
            body = JSON.parse(body);
            if (body.results.length > 0) {
                var raw_address = body.results[0].formatted_address;
                pickup_address = module.exports.formatLocationAddress(raw_address);
            }
        }
        callback(pickup_address);
    });
};

// ADD DAYS TO CURRENT DATE

exports.addDays = function (days) {
    var newDate = new Date();
    newDate.setTime(newDate.getTime() + (86400000 * days)); // add a date
    return new Date(newDate)
};


exports.timeDifferenceInDays = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 86400000);
};

exports.timeDifferenceInWords = function (date1) {
    var diff;
    if (date1 < 60) {
        date1 = date1 + " seconds ago";
    } else if (date1 < 3600) {
        diff = parseInt(date1 / 60);
        if (diff == 1) {
            date1 = diff + " minute ago";
        } else {
            date1 = diff + " minutes ago";
        }
    } else if (date1 < 86400) {
        diff = parseInt(date1 / 3600);
        if (diff == 1) {
            date1 = diff + " hour ago";
        } else {
            date1 = diff + " hours ago";
        }
    } else {
        diff = parseInt(date1 / 86400);
        if (diff == 1) {
            date1 = diff + " day ago";
        } else {
            date1 = diff + " day ago";
        }
    }
    return date1;
};

exports.timeDifferenceInHours = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 3600000);
};

exports.timeDifferenceInMinutes = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 60000);
};

exports.timeDifferenceInSeconds = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 1000);
};


exports.convertTimeIntoLocal = function (date, timezone) {
    if (timezone == undefined || date == '0000-00-00 00:00:00') {
        return date;
    }
    else {
        var newDate = new Date(date);
        timezone = timezone + "";
        var operator = timezone[0];
        var hour = 0;
        var min = 0;
        if (timezone[3] == ":" || timezone[3] == ".") {
            hour = timezone[1] + "" + timezone[2];
            min = timezone[4] + "" + timezone[5];
        } else {
            hour = timezone[1];
            min = timezone[3] + "" + timezone[4];
        }
        hour = parseInt(hour);
        min = parseInt(min);
        var millies = (hour * 60 * 60 * 1000) + (min * 60 * 1000);
        if (operator == "-") {
            newDate.setTime(newDate.getTime() - millies)
        } else {
            newDate.setTime(newDate.getTime() + millies)
        }
        return newDate;
    }
};
exports.firstLetterCapital = function(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}
/**
 * Upload a image to s3 bucket
 */
function uploadImageToS3Bucket(file, isThumb, callback) {

    var path = file.path, filename = file.name, folder = file.s3Folder, mimeType = file.mimeType;
    if (isThumb) {
        path = path + 'thumb/';
        filename = file.thumbName;
        folder = file.s3FolderThumb;
    }
    //var filename = file.name; // actual filename of file
    //var path = file.path; //will be put into a temp directory
    //var mimeType = file.type;

    var accessKeyId = config.s3BucketCredentials.accessKeyId;
    var secretAccessKeyId = config.s3BucketCredentials.secretAccessKey;
    var bucketName = config.s3BucketCredentials.bucket;
    //console.log("UPLOAD", file);

    fs.readFile(path + filename, function (error, fileBuffer) {
        //  console.log("UPLOAD", file_buffer);
        if (error) {
            //  console.error("UPLOAD", error);
            var errResp = {
                response: {
                    message: constants.responseMessage.UPLOAD_ERROR,
                    data: {}
                },
                statusCode: 405
            };
            return callback(errResp);
        }

        //filename = file.name;
        AWS.config.update({accessKeyId: accessKeyId, secretAccessKey: secretAccessKeyId});
        var s3bucket = new AWS.S3();
        var params = {
            Bucket: bucketName,
            Key: folder + '/' + filename,
            Body: fileBuffer,
            ACL: 'public-read',
            ContentType: mimeType,
        };

        s3bucket.putObject(params, function (err, data) {
            //  console.error("PUT", err);
            if (err) {
                console.log(err);
                var error = {
                    response: {
                        message: constants.responseMessage.UPLOAD_ERROR,
                        data: {}
                    },
                    statusCode: 409
                };
                return callback(error);
            }
            else {
                // console.log(data);
                deleteFile(path + filename, function (err) {
                    console.error(err);
                    if (err)
                        return callback(err);
                    else
                        return callback(null);
                })
            }
        });
    });
}
/**
 * Save a file to server
 */
function saveFile(fileData, path, callback) {

    //var path = Path.resolve(".") + "/uploads/" + folderPath + "/" + fileName;

    var file = fs.createWriteStream(path);

    file.on('error', function (err) {
        console.error(err);
        var error = {
            response: {
                message: constants.responseMessage.UPLOAD_ERROR,
                data: {}
            },
            statusCode: 400
        };
        return callback(error);
    });

    fileData.pipe(file);

    fileData.on('end', function (err) {
        if (err) {
            var error = {
                response: {
                    message: constants.responseMessage.UPLOAD_ERROR,
                    data: {}
                },
                statusCode: 400
            };
            return callback(error);
        } else
            callback(null);
    });


}
/**
 * Delete a file from server
 */
function deleteFile(path, callback) {

    fs.unlink(path, function (err) {
        // console.error("delete", err);
        if (err) {
            var error = {
                response: {
                    message: constants.responseMessage.SERVER_ERROR,
                    data: {}
                },
                statusCode: 500
            };
            return callback(error);
        } else
            return callback(null);
    });

}
/**
 * Upload any file to Amazon bucket
 */
exports.uploadFile = function (otherConstants, fileDetails, createThumbnail, callbackParent) {
    var filename = fileDetails.name;
    var TEMP_FOLDER = otherConstants.TEMP_FOLDER;
    var s3Folder = otherConstants.s3Folder;
    var file = fileDetails.file;
    var mimeType = file.hapi.headers['content-type'];
    async.waterfall([
        function (callback) {
            console.log('TEMP_FOLDER + filename'+ TEMP_FOLDER + filename)
            saveFile(file, TEMP_FOLDER + filename, callback);
        },
        function (callback) {
            if (createThumbnail)
                createThumbnailImage(TEMP_FOLDER, filename, callback);
            else
                callback(null);
        },
        function (callback) {
            var fileObj = {
                path: TEMP_FOLDER,
                name: filename,
                thumbName: "Thumb_" + filename,
                mimeType: mimeType,
                s3Folder: s3Folder
            };
            if (createThumbnail)
                fileObj.s3FolderThumb = otherConstants.s3FolderThumb;
            initParallelUpload(fileObj, createThumbnail, callback);
        }
    ], function (error) {
        if (error)
            callbackParent(error);
        else
            callbackParent(null);
    })
};
/**
 * Initiate parallel upload to s3 bucket
 */
function initParallelUpload(fileObj, withThumb, callbackParent) {

    async.parallel([
        function (callback) {
            uploadImageToS3Bucket(fileObj, false, callback);
        },
        function (callback) {
            if (withThumb)
                uploadImageToS3Bucket(fileObj, true, callback);
            else
                callback(null);
        }
    ], function (error) {
        if (error)
            callbackParent(error);
        else
            callbackParent(null);
    })

}
/**
 * Generate filename with extension
 */
exports.generateFilenameWithExtension=  function(oldFilename, newFilename) {
    var ext = oldFilename.substr((~-oldFilename.lastIndexOf(".") >>> 0) + 2);
    return newFilename + '.' + ext;
}
/**
 * Generate random string
 */
exports.randomString = function(num){
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var text = "";
    for(var i = 0; i<num; i++){
        text = text + possible.charAt(Math.floor(Math.random()*possible.length));
    }
    return text;
}
/**
 * Create Thumbnail
 */
function createThumbnailImage(path, name, callback) {
    var gm = require('gm').subClass({imageMagick: true});
    var thumbPath = path + 'thumb/' + "Thumb_" + name;
    //var tmp_path = path + "-tmpPath"; //will be put into a temp directory
    gm(path + name)
        .resize(40, 40, "!")
        .autoOrient()
        .write(thumbPath, function (err) {
            console.error(err);

            if (!err) {
                return callback(null);
            } else {
                var error = {
                    response: {
                        message: constants.responseMessage.SHOW_ERROR_MESSAGE,
                        data: {}
                    },
                    statusCode: 400
                };

                return callback(error);
            }
        })
}
/**
 * Create a hash by specified algorithm
 */
exports.calculateHash = function(algorithm,data){
    var hash = Crypto.createHash(algorithm).update(data).digest('base64');
    return hash;
}
/**
 * Calculate Duration between two Dates
 */
exports.calculateDuration = function(date1,date2){
    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_s = (date2_ms - date1_ms)/1000;
}
/**
 * Joi Fail Action function. Thanks to Shahab
 */
exports.failActionFunction = function (request, reply, source, error) {
    var customErrorMessage = '';
    if (error.output.payload.message.indexOf("[") > -1){
        customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
    }else {
        customErrorMessage = error.output.payload.message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    error.output.payload.message = customErrorMessage;
    delete error.output.payload.validation
    delete error.output.payload.statusCode
    error.output.payload.data = {};
    return reply(error);
};
/**
 *
 */
exports.validateEmail = function(email){
    var regEx = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    if(regEx.test(email)){
        return true;
    }else {
        return false;
    }
}
module.exports.calculateDistance = calculateDistance;
module.exports.contains = contains;
module.exports.getLocalAndUtcTime = getLocalAndUtcTime;
module.exports.formatDateTime = formatDateTime;