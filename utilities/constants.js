/**
 * The node-module to hold the constants for the server
 */

var responseMessage = {
    'PARAMETER_MISSING': 'Some parameter missing.',
    'REGISTRATION_SUCCESSFUL': 'Please check you email for verification.',
    'VERIFICATION_SUCCESSFUL': 'Your Account has been verified successfully',
    'ACCOUNT_ALREADY_VERIFIED' : 'Account you are trying to verify is already verified',
    'ACCOUNT_NOT_VERIFIED': 'You have not verified your account yet.',
    'ACCOUNT_NOT_REGISTERED_PROPERLY': 'You have not registered properly. Please signup again.',
    'INVALID_ACCESS_TOKEN': 'Invalid access token.',
    'INVALID_EMAIL_ID': 'Invalid email id.',
    'LOGIN_ERROR': 'Invalid email or password!',
    'WRONG_PASSWORD': 'Password does not match.',
    'LOGIN_SUCCESSFULLY': 'Logged in successfully.',
    'LOGOUT_SUCCESSFULLY': 'Logged out successfully.',
    'SHOW_ERROR_MESSAGE': 'Show error message.',
    'IMAGE_FILE_MISSING': 'Image file is missing.',
    'ERROR_IN_EXECUTION': 'Error in database execution.',
    'UPLOAD_ERROR': 'Error in uploading.',
    'EMAIL_ALREADY_EXISTS': 'Email already registered',
    'USER_NOT_FOUND': 'The emailId or Facebook Id specified by you was not found in the database',
    'INACTIVE_ACCOUNT': 'Your account is not active or blocked by admin.',
    'INVALID_ACCESS': 'Invalid access. You are not authorized for this operation',
    'NO_DATA': 'No data updated!',
    'NO_DATA_FOUND': 'No data found!',
    'DATA_UPDATED': 'Data Updated',
    'DATA_FOUND': 'Data found',
    'EMAIL_ID_NOT_REGISTERED': 'Email id not registered',
    'STUDENT_NOT_FOUND' : 'Student not found in the database',
    'TUTOR_NOT_FOUND' : 'Tutor  not found in database',
    'COURSE_NOT_FOUND' : 'Course not found in database',
    'SESSION_NOT_FOUND' : 'Session not found in the database',
    'SESSION_EXPIRED' : 'Your session has been expired. Login Again',
    'TUTOR_ALREADY_APPROVED' : 'Tutor you are trying to approve is already approved',
    'TUTOR_APPROVED' : 'The required tutor was approved successfully',
    'SESSION_CANCELLED' : 'The specified session has been cancelled',
    'SESSION_RESCHEDULED' : 'The session has been rescheduled to specified date',
    'SESSION_CREATED' : 'The request was accepted and session was successfully created',
    'REQUEST_REJECTED' : 'The request was rejected by the tutor',
    'REQUEST_ALREADY_ACCEPTED' : 'The request you specified has been already accepted',
    'REQUEST_ACCEPTED' : 'The request was successfully accepted',
    'REQUEST_POSTED' : 'The request was posted successfully',
    'EXTENSION_REQUEST_SENT' :'The request for extension was sent to student',
    'SESSION_EXTENDED' : 'The session was successfully extended',
    'COURSE_ADDED' : 'The course was added successfully to the database',
    'COURSE_ALREADY_EXIST' : 'The course with same code exists already',
    'MAX_COURSES_REACHED' : 'The student has reached the limit of maximum courses',
    'COURSE_ALREADY_ADDED' : 'The course already exists in your profile. Try another course',
    'TUTOR_NOT_APPROVED' : 'The tutor is not approved by Admin yet',
    'MOBILE_ALREADY_EXISTS' : 'The mobile number you specified already exists',
    'SERVER_ERROR' : 'An Error Occurred at the server. We will look forward to it',
    'ALREADY_FAVORITE' : 'The user is already marked as favorite',
    'SESSION_ALREADY_RATED' : 'The session has been already rated',
    'SESSION_RATED' : 'The rating was submitted successfully',
    'TUTOR_NOT_SPECIFIED' : 'No tutors were specified for the request',
    'COURSE_NOT_IN_PROFILE' : 'The course was not found in user profile',
    'REQUEST_NOT_ACCEPTED' : "The request has not been accepted by anyone",
    'REQUEST_SENT' : 'Your request has been successfully sent',
    'INVALID_STRIPE_TOKEN' : 'The stripe token supplied was invalid'
};

module.exports.DURATION_TO_START_SESSION = 1
module.exports.emailWhiteList = ['edu','edu.uk']
module.exports.availableClasses = ['B.Tech','M.Tech','Phd.']
module.exports.ratingCriteria = ['Respectfulness', 'Timeliness', 'Criteria 3', 'Criteria 4']
module.exports.SESSION_TYPES = ['homework','projects'];
module.exports.responseMessage = responseMessage;
module.exports.prices = {
        MIN_PRICE : 10,
        MAX_PRICE : 60
    }
module.exports.MAX_COURSES = 6;
module.exports.MAX_DURATION = 24;
module.exports.fbURL ="http://graph.facebook.com/";
module.exports.DEVICE_TYPES = ['ios','android',''];
