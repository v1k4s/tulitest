/**
 * Created by Vikas Sharma on 7/8/15.
 */
var Dao = require('../dao/DAO');
var Async = require('async');
var Model = require('../models');
var Config = require('../config');
var Util = require('../utilities/util')

/**
 * Create a default admin user with username admin and email specified in Constants
 */

var createAdmin = function(){
    var adminData = Config.admin;
    var err = new Error ;
    var hash = Util.calculateHash('sha256',adminData.password);
    adminData.password = hash;
    Async.waterfall([
        function(callback){
            Dao.getData(Model.User,{roles : {$in : ['admin']}},{_id:1},{lean:true,limit:1},callback);
        },
        function(data,callback){
            if(data.length!=0 || data==null){
                callback(err);
            } else {
               Dao.insertData(Model.User,adminData,callback);
            }
        }
    ],
    function(err,result){
        if(err) {
            console.log('Admin already exists');
        }
        else console.log('Admin created successfully with default credentials');
    })
}

//var checkMissedJobs = function(){
//    Dao.getData()
//}
module.exports = createAdmin;
