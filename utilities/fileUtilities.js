/**
 * Created by Vikas Sharma on 7/17/15.
 */

var Config = require('../config');
var Util = require('./util');
var Async = require('async');
var Path = require('path');

function uploadProfilePicture(profilePicture, filename, callbackParent) {
    var baseFolder = Config.s3BucketCredentials.folder.profilePicture;
    var baseURL = Config.s3BucketCredentials.s3URL + '/' + baseFolder;
    var urls = {};

    Async.waterfall([
            function (callback) {
                var profileFolder = Config.s3BucketCredentials.folder.original;
                var profileFolderThumb = Config.s3BucketCredentials.folder.thumb;
                var randomString = Util.generateRandomString();
                var profilePictureName = Util.generateFilenameWithExtension(profilePicture.hapi.filename, "Profile_" + filename + '_' +  randomString);
                var s3Folder = baseFolder + '/' + profileFolder;
                var s3FolderThumb = baseFolder + '/' +  profileFolderThumb;
                var profileFolderUploadPath = "profilePicture";
                var path = Path.resolve("..") + "/uploads/" + profileFolderUploadPath + "/";

                console.log(path);
                var fileDetails = {
                    file: profilePicture,
                    name: profilePictureName
                };
                var otherConstants = {
                    TEMP_FOLDER: path,
                    s3Folder: s3Folder,
                    s3FolderThumb: s3FolderThumb
                };
                urls.profilePicture = baseURL +'/'+ profileFolder + '/' + profilePictureName;
                urls.profilePictureThumb = baseURL + '/' + profileFolderThumb + '/Thumb_' + profilePictureName;
                Util.uploadFile(otherConstants, fileDetails, true, callback);
            }
        ],
        function (error) {
            if (error)
                callbackParent(error);
            else
                callbackParent(null, urls);
        })
}

function uploadTranscripts(transcripts,userName,callbackParent) {
    var j = 0;
    var i;
    var transcriptUrls = [];
    var tasksToDoInParallel = [];
    transcripts.forEach(function (transcriptData) {
        tasksToDoInParallel.push((function (transcript) {
            return function (internalCB) {
                uploadTranscript(transcript,userName,function (err, transcriptUrl) {
                    if (err) {
                        internalCB(err)
                    } else {
                        if (transcriptUrl)
                            transcriptUrls.push(transcriptUrl);
                        internalCB()
                    }
                })
            }
        })(transcriptData))

    });
    Async.parallel(tasksToDoInParallel, function (err, result) {
        if (err) {
            callbackParent(err)
        } else {
            callbackParent(null,transcriptUrls)
        }
    })
};

function uploadTranscript(transcript,userName,callback) {
    var baseFolder = Config.s3BucketCredentials.folder.tutor;
    var baseURL = Config.s3BucketCredentials.s3URL + '/' + baseFolder;
    var transcriptUrl={};
    var timestamp = Util.getUTCDate();
    Async.waterfall([
            function (callback) {
                var transcriptFolder = Config.s3BucketCredentials.folder.transcript;
                var randomString = Util.randomString(10,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                var transcriptName = Util.generateFilenameWithExtension(transcript.hapi.filename, userName + randomString);
                transcriptUrl.transcriptLink = baseURL + '/' + transcriptFolder + '/' + transcriptName;
                transcriptUrl.addedAt = timestamp;
                transcriptUrl.title = transcript.hapi.filename;
                var s3Folder = baseFolder +'/'+ transcriptFolder;
                var docFolderUploadPath = "tutor/transcripts";
                var path = Path.resolve("..") + "/uploads/" + docFolderUploadPath + "/";
                var fileDetails = {
                    file: transcript,
                    name: transcriptName
                };
                var otherConstants = {
                    TEMP_FOLDER: path,
                    s3Folder: s3Folder
                };
                Util.uploadFile(otherConstants, fileDetails, false, callback);
            }
        ],
        function (error) {
            if (error){  callback(error);}
            else{callback(null, transcriptUrl);}

        })
};

module.exports = {
    uploadProfilePicture :  uploadProfilePicture,
    uploadTranscripts : uploadTranscripts
}