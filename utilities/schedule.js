/**
 * Created by Vikas Sharma on 7/14/15.
 */

var Config = require('../config')
var Agenda = require('agenda');
var agenda = new Agenda();
var Dao = require('../dao/DAO');
var Async = require('async');
var Model = require('../models');



agenda.database(Config.db.test, 'agendaJobs');
agenda.processEvery('5 minutes');

module.exports.scheduleSessionStart = function(sessionId, when){
    var name = sessionId +  'start'
    agenda.define(name,function(job,done){                               // data = {sessionId}
    if(job.attrs.data.sessionId){
        console.log('Scheduling job with sessionId', sessionId);
        var sessionId = job.attrs.data.sessionId;
        Async.waterfall([
            function(callback){
                Dao.getData(Model.Session,{_id : sessionId, status : 'scheduled'},{},{lean : true, limit : 1},callback);
            },
            function(data,callback){
                if(data.length == 0){
                    var err = new Error("No scheduled session Found");
                    callback(err);
                }else{
                    console.log('Session started');
                    Dao.updateData(Model.Session,{_id : sessionId},{status : 'ongoing'},{},callback);
                }
            }
            ],
        function(err, result){
            if(err) done(err)
            else done();
        })
    } else {
        var err = new Error("The session Id was not specified");
        done();
    }
     })

    agenda.schedule(when,name,{sessionId : sessionId});
    agenda.start();
}


module.exports.cancelSession = function(sessionId,callbackController) {
    var name = sessionId + 'start';
    agenda.cancel(name);
    var name = sessionId + 'end';
    agenda.cancel(name,callbackController);
}


module.exports.scheduleSessionEnd = function(sessionId ,when){
    var name = sessionId +  'end'
    agenda.define(name,function(job,done){                               // data = {sessionId}
        if(job.attrs.data.sessionId){
            var sessionId = job.attrs.data.sessionId;
            Async.waterfall([
                    function(callback){
                        Dao.getData(Model.Session,{_id : sessionId, status : 'current'},{},{lean : true, limit : 1},callback);
                    },
                    function(data,callback){
                        if(data.length == 0){
                            var err = new Error("No current session Found");
                            callback(err);
                        }else{
                            Dao.updateData(Model.Session,{_id : sessionId},{status : 'completed'},{},callback);
                        }
                    }
                ],
                function(err, result){
                    if(err) done(err)
                    else done();
                })
        } else {
            var err = new Error("The session Id was not specified");
            done();
        }
    })

    agenda.schedule(when,sessionId,{sessionId : sessionId});
    agenda.start();
}