/**
 * Created by Vikas Sharma on 7/14/15.
 */
var redisClient = require('redis').createClient(6379);
var Jwt = require('jsonwebtoken');
var Config = require('../config');
var Async = require('async');
redisClient.on('error', function (err) {
    console.log('Error ' + err);
});

redisClient.on('connect', function () {
    console.log('Redis is ready');
});


/**
 * Creates a token in redis database
 */

exports.createToken = function(key,value){
              redisClient.get(key,function(err,data){
                  if((!data) || err){
                      redisClient.set(key,value);
                  } else {
                      var n = redisClient.del(key);
                      console.log(n);
                      redisClient.set(key,value);
                  }
              });
        }

/**
 * Expires a token and deletes it from Redis database
 */

exports.expireToken = function(token,callbackController){
    var decodedToken = {};
    Async.waterfall([
            function(callback){
                Jwt.verify(token,Config.secret.key,callback);
            },
            function(data,callback){
                console.log(data);
                decodedToken = data;
                redisClient.get(data.id,callback);
            },
            function(data,callback){
                if(!data) callback(new Error());
                else redisClient.del(decodedToken.id);
                callback();
            }
        ],
    function(err,data){
        if(err) callbackController(err);
        else callbackController();
    })
};

/**
 * Verifies the token in the Redis database and returns True if found
 */

exports.verifyToken = function(decodedToken,callback){
    var tokenToEncode = decodedToken;
    var token = Jwt.sign(tokenToEncode,Config.secret.key);
    redisClient.get(decodedToken.id, function (err,reply) {
        if(err){
            return  callback(err);
        } else if (reply == token){
            return  callback(null,reply);
        } else {
            return callback(new Error());
        }
    })
}