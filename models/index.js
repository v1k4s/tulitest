/**
 * Created by Vikas Sharma on 7/2/15.
 */

var user = require('./user');
var request = require('./request');
var session = require('./session');
var payment = require('./payment');
var course = require('./course')

module.exports = {
    User : user,
    RequestForStudent : request.requestForStudent,
    RequestForTutor : request.requestForTutor,
    Session : session,
    Payment : payment,
    Course : course
}