/**
 * Created by Vikas Sharma on 7/2/15.
 */

var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Constants = require('../utilities/constants')

var requestForStudentSchema = new Schema({
    parentRequestId: {
        courseId: {                                                   // parent RequestId to which tutor is responding
            type: Mongoose.Schema.Types.ObjectId,
            ref: 'requestForTutor',
            required: true
        }
    },
    studentId: {                                                                     // studentId which is addressed
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    tutorId: {                                                                      // tutorId which is addressed
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    location: {type: String},
    fee: {type: Number, min: 0},
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},
    notes: {type: String},                                                          // Any notes added by the tutor
    type: {type: String, enum: ['personal', 'general']}                             // Whether it is a reply to general request or modified personal request
})

var requestForTutorSchema = new Schema({                                            // CourseId of the course for which request is posted
    courseId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'course'
    },
    topics: {type: [String]},
    location: {type: String},                                                      // Location of the requested session
    description: {type: String},                                                  // Description of the request
    noOfStudents: {type: Number},
    studentId: {
        type: Mongoose.Schema.Types.ObjectId,                                      // ID of student
        ref: 'user'
    },
    tutorId: [{
        type: Mongoose.Schema.Types.ObjectId,                                      // Tutor IDs of tutors from which student wants to study
        ref: 'user'
    }],
    requestType: {type: String, enum: ['general', 'personal']},                    // Type of request whether General/personal request
    status: {type: String, enum: ['pending', 'accepted', 'rejected', 'modified', 'scheduled'], default: 'pending'}, // Status of the current request
    requestTime: {type: String, enum: ['onDemand', 'scheduled']},                  // Whether on demand or Scheduled request
    sessionType: {type: String, enum: Constants.SESSION_TYPES},                  // Type of the session
    noOfInterests: {type: Number, default: 0},                                  // No of interested tutors
    startTime: {type: Date},                                                        // Starting time of the session
    endTime: {type: Date},                                                          // Ending time of the session
    duration: {type: Number, max: Constants.MAX_DURATION},                       // Maximum duration of the request
    feeLower: {type: Number},                                                       // lower fee
    feeUpper: {type: Number},                                                     // Upper fee
    postedOn: {type: Date}
})

var requestForStudent = Mongoose.model('requestForStudent', requestForStudentSchema);
var requestForTutor = Mongoose.model('requestForTutor', requestForTutorSchema);

exports.requestForStudent = requestForStudent;
exports.requestForTutor = requestForTutor;