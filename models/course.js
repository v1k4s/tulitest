/**
 * Created by Vikas Sharma on 7/2/15.
 */
var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Constants = require('../utilities/constants')

courseSchema = new Schema({
    code: {type: String, required: true, unique: true},                            // Course Code
    title: {type: String, required: true},                                          // Title of the Course
    topics: {type: [String]},                                                        // Topics covered in the course
    desc: {type: String},				                                             // Description of the Course
    //tutors : [{
    //    type : Mongoose.Schema.Types.ObjectId,
    //    ref : 'user'
    //}],						                                                           // ID of Tutors who can teach the course
    //students : [{
    //    type : Mongoose.Schema.Types.ObjectId,
    //    ref : 'user'
    //}],                                                                                 // Id of Students who are studying the course and want to learn that course
    //class : {type : String} //enum : Constants.availableClasses},                         // Classes in which the course is taught
    ////university : {type : String}                                                      // University in which the course is taught
})


var course = Mongoose.model('course', courseSchema);

module.exports = course;