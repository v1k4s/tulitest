/**
 * Created by Vikas Sharma on 7/2/15.
 */

var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;
var Constants = require('../utilities/constants');

var sessionSchema = new Schema({                                                                // Course ID of the session
    courseId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'course'
    },
    topics: {type: [String], default: []},						                                            // Topics to be covered during the session
    studentId: {
        type: Mongoose.Schema.Types.ObjectId,                                                  // Student ID of students who wants to take the tution
        ref: 'user'
    },
    tutorId: {                                                                                 // Tutor ID of tutor who will teach session
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    location: {type: String},                                                                 // Location of the session
    sessionType: {type: String, enum: Constants.SESSION_TYPES},
    noOfStudents: {type: Number},                                                             // Number of students taking part in the session
    startTime: {type: Date, required: true},			                                            // Starting time of the session
    endTime: {type: Date, required: true},  			                                            // ending time of the session
    status: {
        type: String,
        enum: ['scheduled', 'ongoing', 'completed', 'extended', 'pending', 'expired'],
        required: true
    }, // status of the session out of {available, scheduled, ongoing, expired}
    extendTime: {type: Number},                                                               // Number of hours the session is extended
    sessionCost: {type: Number},                                                                 // the price to which both tutor and student have agreed
    ratingByStudent: [{type: Number, min: 1, max: 5}],                                         // rating by the student
    ratingByTutor: [{type: Number, min: 1, max: 5}]                                            // rating by the tutor
})

var session = Mongoose.model('session', sessionSchema);

module.exports = session;