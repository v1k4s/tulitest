/**
 * Created by Vikas on 7/2/15.
 */

var Mongoose = require('mongoose');
var Schema = Mongoose.Schema;

var paymentSchema = new Schema({
    sessionId: {                                                                // Session corresponding to which payment made
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'session'
    },
    studentId: {                                                                // Student Id which made the payment
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    tutorId: {                                                                 // tutor Id which made the payment
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    paymentDate: {type: Date},                                                   // When was the payment received
    status: {type: String, enum: ['pending', 'completed'], default: 'pending'}
})

var payment = Mongoose.model('payment', paymentSchema);

module.exports = payment;