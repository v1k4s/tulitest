/**
 * Created by Vikas Sharma on 7/2/15.
 */


var Mongoose = require('mongoose');
var Constants = require('../utilities/constants');

var Schema = Mongoose.Schema;
//var cardSchema = new Schema({
//    stripeCustomerId : {type:String},
//    lastFourDigits : {type:Number},
//})
//var Availability = {
//    monday : [{type : Number, min : 0, max : 23}],
//    tuesday : [{type : Number, min : 0, max : 23}],
//    wednesday : [{type : Number, min : 0, max : 23}],
//    thursday : [{type : Number, min : 0, max : 23}],
//    friday : [{type : Number, min : 0, max : 23}],
//    saturday : [{type : Number, min : 0, max : 23}],
//    sunday : [{type : Number, min : 0, max : 23}]
//}
var Availability = [{
    dayId: {type: Number, min: 1, max: 7},
    bit: [{type: Number, min: 0, max: 1}]
}]

var userSchema = new Schema({
    roles: {type: [String], enum: ['student', 'tutor', 'admin'], required: true},     // Roles out of {student,tutor,admin}
    firstName: {type: String, required: true},                                       // First Name
    lastName: {type: String, required: true},                                        // Last Name
    fbRegisterFlag: {type: Number, min: 0, max: 1, required: true},                              // Whether facebook register
    //userName : {type : String , required : true, unique : true},                      // User Name
    facebookEmail: {type: String, unique: true, sparse: true},                      // list of emails associated with account
    primaryEmail: {type: String, required: true, unique: true},                      // primary email for communication
    year: {type: String},                                                            // doubtful
    mobileNumber: {type: String, required: true, unique: true},                     // Mobile Number
    password: {type: String},                                                        // use hashed password(sha256)
    profilePicture: {type: String, required: true},                                 // Store the URL of AWS bucket to which profile pic is uploaded
    profilePictureThumb: {type: String, required: true},                          // Thumbnail of the profile picture
    fbId: {type: String, unique: true, sparse: true},
    registrationDate: {type: Date},                                                  // date of registration of the student
    college: {type: String},                                                         // Bucket URLs of the transcript uploaded by the Tutor
    transcripts: [{
        transcriptLink: {type: String},
        addedAt: {type: Date},
        title: {type: String}
    }],
    majors: {type: String, default: ''},                                         // Majors
    minors: {type: String, default: ''},                                          // Minors
    //tutorClasses : [{                                                                 // Classes which can be tutored by tutor
    //    class : {type :String},
    //    gpa : {type: Number}
    //}],
    courses: {                                                                       // List of Courses
        isStudying: [{
            type: Mongoose.Schema.Types.ObjectId,                                      // Courses which student study
            ref: 'course',
            unique: true
        }],
        canTeach: [
            {
                courseId: {
                    type: Mongoose.Schema.Types.ObjectId,                                      // Courses which tutor is verified to teach
                    ref: 'course',
                    unique: true,
                    sparse: true
                },
                courseFee: {type: Number, min: 0}
            }
        ]
    },
    isVerified: {type: Boolean, default: true},                                       // for email verification purposes
    isApprovedTutor: {type: Boolean, default: true},
    studentRating: {type: Number, default: 0},                                       // for Rating as student
    tutorRating: {type: Number, default: 0},                                         // for Rating as a tutor
    lastLogin: {type: Date},                                                          // Storing date of the last login
    deviceType: {type: String, enum: Constants.DEVICE_TYPES},                        // Storing the device related info from the last login
    deviceToken: {type: String},                                                    // Registration Id of the device to send notifications
    appVersion: {type: String},                                                     // App version to do forceful updates
    favorites: {
        tutors: [{
            type: Mongoose.Schema.Types.ObjectId,                                                  // Favorite tutors
            ref: 'user'
        }],
        students: [{
            type: Mongoose.Schema.Types.ObjectId,                                                  // Favorite students
            ref: 'user'
        }]
    },
    savedCards: [{type: String}],                                                    // Stripe Customer ids of student
    bioDescription: {type: String},                                                 // A brief description of the user
    fee: {type: Number},
    noOfCompletedSessions: {type: Number, default: 0},                             // Number of completed sessions
    availability: Availability
})

var user = Mongoose.model('user', userSchema);
module.exports = user;