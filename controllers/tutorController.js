/**
 * Created by Vikas Sharma on 7/6/15.
 */
var Constants = require('../utilities/constants');
var Model = require('../models');
var Dao = require('../dao/DAO');
var Async = require('async');
var Util = require('../utilities/util');
var Jwt = require('jsonwebtoken');
var Config = require('../config');
var Validation = require('../utilities/validationFunctions');
var Schedule = require('../utilities/schedule');
var Logger = require('../utilities/logger');
var _ = require('lodash');

/**
 * Function to accept personal request by tutor
 */
exports.acceptRequest = function(requestData, callbackRoute)    {
    var tutorId = requestData.id,
        requestId = requestData.requestId,
        //tutorFee,
        //insertData={},
        response = {},
        errorResponse = {
            response : response
        }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                // tutorFee = _.filter(data[0].courses.canTeach)
                // tutorFee = data[0].courses.canTeach.fee
                Dao.getData(Model.RequestForTutor,{_id : requestId,tutorId : tutorId ,requestType : 'personal'},{},{lean:true},callback);
            },
            function(data,callback){
                if(data==null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else if(data[0].status != 'pending') {
                    response.message = Constants.responseMessage.REQUEST_ALREADY_ACCEPTED;
                    response.data = {};
                    errorResponse.statusCode = 204;
                    callback(errorResponse);
                } else {
                    //insertData = {
                    //    courseId: data[0].courseId,
                    //    topics: data[0].topics,
                    //    studentId: data[0].studentId,
                    //    tutorId: tutorId,
                    //    location: data[0].location,
                    //    noOfStudents: data[0].noOfStudents,
                    //    status: 'scheduled',
                    //    sessionCost: tutorFee
                    //}
                    //if(data[0].requestTime = 'scheduled'){
                    //        insertData.startTime= data[0].startTime;
                    //        insertData.endTime = data[0].endTime;
                    //} else {
                    //        var currentTime = Util.getUTCDate();
                    //        currentTime.setUTCHours(currentTime.getUTCHours() + Constants.DURATION_TO_START_SESSION);
                    //        insertData.startTime = currentTime;
                    //        var duration = data[0].duration;
                    //        var hoursToAdd = Math.floor(duration);
                    //        var minutesToAdd =  Math.floor((duration - hoursToAdd)*60);
                    //        currentTime.setUTCHours(currentTime.getUTCHours() + hoursToAdd);
                    //        currentTime.setUTCMinutes(currentTime.getUTCMinutes() + minutesToAdd);
                    //        insertData.endTime = currentTime;
                    //}
                    var updateData = {
                        status : 'accepted',
                        noOfInterests : 1,
                        $pull : {tutorId : {$ne : tutorId}},
                    }
                    Dao.updateData(Model.RequestForTutor,{_id : requestId},updateData,{limit :1},callback);
                }
            },
            function(data,callback){
                var success = {
                    response : {
                        message : Constants.responseMessage.REQUEST_ACCEPTED,
                        data : {}
                    },
                    statusCode : 201
                }
                callback(null,success);
            }
            //,
            //function(data,callback){
            //    Dao.insertData(Model.Session,insertData,callback);
            //}
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                //Schedule.scheduleSessionStart(result._id,result.startTime);
                //Schedule.scheduleSessionEnd(result._id,result.endTime);
                /*
                 * Send push notification to the user
                 */
                callbackRoute(null,result);
            }
        })
}
/**
 * Function to reject request by tutor
 */
exports.rejectRequest = function(requestData, callbackRoute){
    var requestId = requestData.requestId;
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.RequestForTutor,{_id : requestId, requestType : 'personal', status : 'pending', tutorId : tutorId},{},{lean : true, limit : 1},callback)
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    var updateData = {
                        $pull : {tutorId : tutorId}
                    }
                    var tutorArray = JSON.stringify(data[0].tutorId);                // Array of tutors from the request
                    if(tutorArray.length == 1){
                        updateData.status = 'rejected'
                    }
                    Dao.updateData(Model.RequestForTutor,{_id : requestId},updateData,{limit : 1},callback);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.REQUEST_REJECTED;
                response.data = {};
                var result = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,result);
            }
        } )
}
/**
 * Function to send request to the student in response to general posts
 */
exports.sendRequest = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var requestId = requestData.requestId;
    var response = {};
    var errorResponse = {
        response : response
    }
    //var insertData;
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.RequestForTutor,{_id : requestId, requestType : 'general', status : 'pending'},{},{lean : true , limit : 1},callback);
            },
            function(data,callback){
                if(data.length ==0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404 ;
                    callback(errorResponse);
                } else {
                    //insertData = {
                    //    parentRequestId : requestId,
                    //    fee : requestData.fee,
                    //    tutorId : tutorId,
                    //    studentId : data[0].studentId,
                    //    startTime : data[0].startTime,
                    //    endTime : data[0].endTime,
                    //    type : 'general'
                    //}
                    Dao.updateData(Model.RequestForTutor,{_id : requestId},{$inc : {noOfInterests : 1}, $push : {tutorId : tutorId}},{lean : true, limit : 1},callback);
                }
            }
            // ,
            //function(data,callback){
            //    Dao.insertData(Model.RequestForStudent,insertData,callback)
            //}
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.REQUEST_POSTED;
                response.data = result;
                var success = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,success);
            }
        })

}
/**
 * Function to modify request sent by the student
 */
exports.modifyRequest = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var requestId = requestData.requestId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.RequestForTutor,{_id : requestId, requestType : 'personal',requestTime : 'scheduled',status : 'pending'},{},{lean : true , limit : 1},callback);
            },
            function(data,callback){
                if(data.length ==0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404 ;
                    callback(errorResponse);
                } else {
                    var endTime = new Date();
                    if(requestData.startTime) {
                        endTime = startTime;
                        endTime.setUTCHours(endTime.getUTCHours()+duration);
                    }
                    var insertData = {};
                    insertData.parentRequestId = requestId;
                    insertData.tutorId  = tutorId,
                        insertData.studentId = data[0].studentId;
                    if(requestData.location)  insertData.location = requestData.location;
                    if(requestData.startTime) insertData.startTime = requestData.startTime.toISOString(); else insertData.startTime = data[0].startTime;
                    if(requestData.notes) insertData.notes = requestData.notes;
                    if(requestData.startTime)insertData.endTime = endTime; else insertData.endTime = data[0].endTime;
                    insertData.type = 'personal';
                    Dao.insertData(Model.RequestForStudent,insertData,callback)
                }
            },
            function(data,callback){
                Dao.updateData(Model.RequestForTutor,{_id : requestId}, {status : 'modified',noOfInterests : 1},{limit : 1},callback)
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.REQUEST_POSTED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,success);
            }
        })

}
/**
 * View the favorite students of tutor
 */
exports.myFavorites = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response :response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : tutorId},{favorites:1},{lean:true,limit :1},callback);
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    var students = data[0].favorites.students;
                    callback(null,students);
                }
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : {$in : data}},{firstName : 1, lastName : 1, rating : 1,profilePicture : 1},{lean:true,limit:1},callback)
            },
            function(data,callback){
                if(data.length == 0 ){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })

}
/*
 Lists the earnings of a tutor
 */
exports.myEarnings = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    var replyData = {};
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var collectionOptions = {path : 'studentId' , select : 'firstName lastName profilePicture', lean : true}
                Dao.getDataWithReference(Model.Payment,{tutorId : tutorId},{studentId:1,tutorId:1,sessionId:1,paymentDate:1},collectionOptions,callback)
            },
            function(data,callback){
                if(data==null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    replyData = data[0];
                    Dao.getData(Model.User, {_id: replyData.studentId}, {firstName: 1, lastName: 1}, {
                        lean: true,
                        limit: 1
                    }, callback)
                }
            }
            ,function(data,callback) {
                replyData.studentFirstName = data[0].firstName;
                replyData.studentLastName = data[0].lastName;
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = replyData;
                var success = {
                    response: response,
                    statusCode: 200
                }
                callback(null, success);
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })
}
/**
 * Lists the sessions of the user
 */
exports.mySessions = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var query = {
        tutorId : tutorId
    };
    if(requestData.studentId) query.studentId = requestData.studentId;
    if(requestData.status) query.status = requestData.status;
    if(requestData.courseId) query.courseId = requestData.courseId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var collectionOptions = { path : 'studentId courseId', select : 'firstName lastName profilePicture code title', lean: true}
                Dao.getDataWithReference(Model.Session,query,{},collectionOptions,callback)
            },
            function(data,callback) {
                if (data == null || data.length == 0) {
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    var success = {
                        response : {
                            message : Constants.responseMessage.DATA_FOUND,
                            data : data
                        },
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else{
                callbackRoute(null,result);
            }
        })

}
/**
 * Lists the requests for the tutor
 */
exports.myRequests = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : tutorId},{"courses.canTeach.courseId" : 1},{lean : true, limit:1},callback);
            },
            function(data,callback){
                var query = {
                    $or : [{tutorId : tutorId},{tutorId : {$size : 0}}],
                    status : 'pending'
                }
                if(requestData.courseId) {
                        query.courseId = requestData.courseId
                }
                else {
                    var courseArray = [];
                    for(var i=0;i<data[0].courses.canTeach.length;i++){
                        courseArray.push(data[0].courses.canTeach[i].courseId);
                    }
                   query.courseId = {$in : courseArray}
                }
                var collectionOptions = { path : "studentId", select : "firstName lastName profilePictureThumb", lean : true}
                Dao.getDataWithReference(Model.RequestForTutor,query,{studentId : 1, requestType : 1, requestTime : 1},collectionOptions,callback);
            },
            function(data,callback) {
                if (data == null || data.length == 0) {
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);

                } else {
                    var replyData = _.groupBy(data,'requestType');
                    if(!replyData.personal){
                        replyData.personal = []
                    }
                    if(!replyData.general){
                        replyData.general = []
                    }
                    var success = {
                        response: {
                            data: replyData,
                            message: Constants.responseMessage.DATA_FOUND
                        },
                        statusCode: 200
                    }
                    callback(null, success);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err)
            else callbackRoute(null,result);
        })
}
/*
 * List Courses in which the tutor can teach
 */
exports.myCourses = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.User,{_id:tutorId},{courses:1},{lean:true,limit:1},callback);
            },
            function(data,callback){
                if(data.length ==0){
                    var response = {
                        message : Constants.responseMessage.NO_DATA_FOUND,
                        data : {}
                    };
                    var errorResponse = {
                        response : response,
                        statusCode : 400
                    };
                    callback(errorResponse);
                }
                var courseArray = []
                for(var i=0;i<data[0].courses.canTeach.length;i++){
                  courseArray.push(data[0].courses.canTeach[0].courseId);
                }
                Dao.getData(Model.Course,{_id :{$in: courseArray}},{code:1,title:1,topics:1,desc:1},{lean:true},callback);
            },
            function(data,callback){
                var response = {};
                var errorResponse = {
                    response : response
                };
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.response = response;
                    errorResponse.statusCode = 404;                                          // Not found Status Code
                    callback(errorResponse);
                } else {
                    response = {
                        message : 'Course details for tutor',
                        data : data
                    }
                    success = {
                        response : response,
                        statusCode : 200

                    }
                    callback(null,success);
                }
            }
        ], function(err,success){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,success);
            }
        }
    )
}
/**
 * View the details of a particular request
 */
exports.viewRequest = function(requestData,callBackRoute){
    var tutorId = requestData.id;
    var requestId = requestData.requestId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback)
            },
            function(data,callback){
                var collectionOptions = { path : 'studentId courseId', select : 'firstName lastName studentRating profilePicture studentRating college code title', lean : true , limit : 1};
                var projection = {
                    studentId : 1,
                    courseId : 1,
                    startTime : 1,
                    endTime : 1,
                    feeLower : 1,
                    feeUpper : 1,
                    requestType : 1 ,
                    requestTime : 1,
                    description : 1,
                    location : 1,
                    noOfStudents : 1
                }
                Dao.getDataWithReference(Model.RequestForTutor,{_id : requestId, $or : [{tutorId : tutorId},{tutorId : {$size : 0}}]},projection,collectionOptions,callback);
            },
            function(data,callback){
                if(data==null || data.length == 0) {
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data[0]
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callBackRoute(err);
            } else {
                callBackRoute(null, result);
            }
        })
}
/**
 * Cancel a scheduled session
 */
exports.cancelSession = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
               Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId, tutorId : tutorId, status:'scheduled'},{},{lean : true, limit :1},callback);
            },
            function(data,callback){
                if(data == null || data.length ==0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Schedule.cancelSession(sessionId);
                    Dao.deleteData(Model.Session,{_id:sessionId},callback);
                }
            }
        ],
        function(err){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.SESSION_CANCELLED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,success);
            }
        })
}
/*
 * Reschedule a session
 */
exports.rescheduleSession = function(requestData,callbackRoute){
    var tutorId = requestData.tutorId;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Dao.getData(Model.Session,{_id :sessionId,tutorId : tutorId},{},{lean : true,limit : 1},callback)
            },
            function(data,callback){
                var startTime = requestData.startTime;
                var endTime = startTime;
                endTime.setUTCHours(endTime.getUTCHours()+duration);
                var updateData = {
                    startTime : startTime,
                    endTime : endTime
                }
                Schedule.cancelSession(sessionId);
                Schedule.scheduleSessionStart(sessionId,updateData.startTime);
                Schedule.scheduleSessionEnd(sessionId,updateData.endTime);
                Dao.updateData(Model.Session,{_id: sessionId},updateData,{},callback)
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.SESSION_RESCHEDULED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,success);
            }
        })
}
/**
 *  Function to extend the current session
 */
exports.extendSession = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var sessionId = requestData.sessionId;
    var extendTime = requestData.extendTime;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function (data,callback) {
                Dao.getData(Model.Session,{_id: sessionId,tutorId: tutorId,status : {$in : ['ongoing','scheduled']}},{},{limit : 1, lean : true},callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.updateData(Model.Session,{_id : sessionId},{status : 'extended', extendTime : extendTime},{},callback);
                }
            },
            function(data,callback){
                if(data.n != 0){
                    response.message = Constants.responseMessage.EXTENSION_REQUEST_SENT;
                    response.data = {};
                    var success = {
                        response : response,
                        statusCode : 201
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err)
            else callbackRoute(null,result);
        })
}
/**
 * function to see the available courses to the tutor
 */
exports.availableCourses = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback)
            },
            function(data,callback){
                var options = {lean : true}
                if(requestData.limit) options.limit = requestData.limit;
                if(requestData.offset) options.skip = requestData.offset;
                var courseArray = [];
                for(var i=0;i<data[0].courses.canTeach.length;i++){
                    courseArray.push(data[0].courses.canTeach[0].courseId);
                }
                Dao.getData(Model.Course,{_id : {$nin: courseArray} },{code: 1 , title : 1, desc : 1},options,callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.COURSE_NOT_FOUND;
                    response.data ={};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    var success = {
                        response : {
                            message : Constants.responseMessage.DATA_FOUND,
                            data : data
                        },
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}
/**
 * function to add the course to tutor profile
 */
exports.addCourse =  function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var courseId = requestData.courseId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback)
            },
            function(data,callback){
                var canTeach = JSON.stringify(data[0].courses.canTeach);
                if(canTeach.indexOf(courseId) != -1){
                    response.message = Constants.responseMessage.COURSE_ALREADY_ADDED;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else{
                    Dao.getData(Model.Course,{_id : courseId },{},{lean : true, limit : 1},callback);
                }
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.COURSE_NOT_FOUND;
                    response.data ={};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.updateData(Model.User,{_id : tutorId},{$push :{"courses.canTeach" : courseId}},{limit : 1},callback);
                }
            },
            function(data,callback){
                Dao.updateData(Model.Course,{_id : courseId},{$push : {tutors : tutorId}},{limit:1},callback);
            },
            function(data,callback){
                var success = {
                    response : {
                        message : Constants.responseMessage.COURSE_ADDED,
                        data : {}
                    },
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}
/**
 * Function to set availability of the tutor
 */
exports.setAvailability = function(requestData,callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var updateData = {}
                //if(requestData.monday && requestData.monday != []) updateData.availability.monday = requestData.monday;
                //if(requestData.tuesday && requestData.tuesday != []) updateData.availability.tuesday = requestData.tuesday;
                //if(requestData.wednesday && requestData.wednesday != []) updateData.availability.wednesday = requestData.wednesday;
                //if(requestData.thursday && requestData.thursday != []) updateData.availability.thursday = requestData.thursday;
                //if(requestData.friday && requestData.friday != []) updateData.availability.friday = requestData.friday;
                //if(requestData.saturday && requestData.saturday != []) updateData.availability.saturday = requestData.saturday;
                //if(requestData.sunday && requestData.sunday != []) updateData.availability.sunday = requestData.sunday;
                updateData.availability = requestData.availability;
                Dao.updateData(Model.User,{_id : tutorId},updateData,{lean: true, limit : 1}, callback);
            },
            function(data,callback){
                response.message = Constants.responseMessage.DATA_UPDATED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err, result){
            if(err) callbackRoute(err);
            else callbackRoute(null, result);
        })
}
/**
 * View my availability
 */
exports.viewAvailability = function(requestData, callbackRoute){
    var tutorId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var replyData = {
                    availability : {}
                };
                //if(requestData.day){
                //    var key = requestData.day;
                //    var tutorData = data[0].availability;
                //    console.log(tutorData);
                //    replyData.availability[key] = tutorData[key] || [];
                //    response.message = Constants.responseMessage.DATA_FOUND;
                //    response.data = replyData;
                //    var success ={
                //        response :response,
                //        statusCode : 200
                //    }
                //} else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data[0].availability || [];
                    var success = {
                        response: response,
                        statusCode: 200
                    }
                //}
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(result);
        })
}
/**
 * Provide feedback for the session and rate the student
 */
exports.rateStudent = function(requestData, callbackRoute){
    var tutorId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId},{studentId : 1, ratingByTutor : 1},{lean : true, limit : 1},callback)
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else if(data[0].ratingByTutor != []){
                    response.message = Constants.responseMessage.SESSION_ALREADY_RATED;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.getData(Model.User,{_id : data[0].studentId},{studentRating : 1},{lean : true, limit : 1},callback);
                }
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    var ratingToAdd = (requestData.rating[0] + requestData.rating[1] + requestData.rating[2] + requestData.rating[3])/4;
                    var studentRating = (data[0].studentRating + ratingToAdd) /2;
                    Dao.updateData(Model.User,{_id : data[0]._id},{studentRating : studentRating},{limit : 1},callback);
                }
            },
            function(data,callback){
                Dao.updateData(Model.Session,{_id : sessionId},{ratingByTutor : requestData.rating},{limit : 1},callback);
            },
            function(data,callback){
                response.message =  Constants.responseMessage.SESSION_RATED;
                response.data = {}
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}
/**
 * Schedule a completed session again
 */
exports.scheduleAgain = function(requestData, callbackRoute){
    var tutorId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {}
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId, tutorId : tutorId, status : "completed"},{_id : 0, __v : 0,studentRating : 0,tutorRating : 0,extendTime : 0},{lean : true, limit : 1},callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 204;
                    callback(errorResponse);
                } else {
                    var insertData = data[0];
                    if(requestData.location) insertData.location = requestData.location;
                    if(requestData.startTime) insertData.startTime = requestData.startTime.toISOString();
                    if(requestData.duration){
                        var hoursToAdd = Math.floor(requestData.duration);
                        var minutesToAdd = Math.floor((requestData.duration - hoursToAdd)*60);
                        var endTime = new Date(requestData.startTime);
                        endTime.setUTCHours(endTime.getUTCHours() + hoursToAdd);
                        endTime.setUTCMinutes(endTime.getUTCMinutes() + minutesToAdd);
                        insertData.endTime = endTime.toISOString();
                    }
                    insertData.status = 'pending';
                    Dao.insertData(Model.Session,insertData, callback);
                }
            },
            function(data,callback){
                // Send push notification to student for confirmation
                response.message = Constants.responseMessage.REQUEST_SENT;
                response.data = data;
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })

}
/**
 * Confirm schedule again by student
 */
exports.confirmScheduleAgain = function(requestData, callbackRoute){
    var tutorId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {}
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId, tutorId : tutorId, status : "pending"},{},{lean : true, limit : 1},callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {}
                    errorResponse.statusCode = 204;
                    callback(errorResponse);
                } else {
                    // Schedule sessions with Agenda module
                    // *********
                    // update status to scheduled
                    Dao.updateData(Model.Session,{_id : sessionId},{status : "scheduled"},{limit : 1},callback);
                }
            },
            function(data,callback){
                response.message = Constants.responseMessage.SESSION_CREATED;
                response.data = {}
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}