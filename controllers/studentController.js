/**
 * Created by Vikas Sharma on 7/3/15.
 */
var Constants = require('../utilities/constants');
var Model = require('../models');
var Dao = require('../dao/DAO');
var Async = require('async');
var Util = require('../utilities/util');
var Jwt = require('jsonwebtoken');
var Config = require('../config');
var Validation = require('../utilities/validationFunctions');
var Stripe = require('stripe')(Config.stripe.testKey);
var Logger = require('../utilities/logger');
var Schedule = require('../utilities/schedule')
var _ = require("lodash");
var Mongoose = require('mongoose');

/*
 Post a new Personal/general request
 */

exports.newRequest = function(requestData,callbackRoute){
    var studentId = requestData.id;
    if(requestData.tutorId) var tutorId = requestData.tutorId;
    var courseId = requestData.courseId;
    Logger.debug(requestData,"Request before passing on to Controller");
    Async.waterfall([
            function(callback){                                                                                                                     //function to check student Id
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){                                                                                                                 //function to check tutor ID
                if(requestData.requestType=='personal'){
                    if(tutorId.length == 0){
                        var response = {
                            message : Constants.responseMessage.TUTOR_NOT_SPECIFIED,
                            data : {}
                        }
                        var errorResponse = {
                            response : response,
                            statusCode : 400
                        }
                        callback(errorResponse);
                    }else {
                        Dao.getData(Model.User,{_id : {$in: tutorId}, "courses.canTeach" : { $elemMatch : {courseId : courseId} }},{},{lean : true},callback);  // Check if all the tutorIds are valid and they teach that course
                    }
                }else {
                    if(requestData.tutorId) delete requestData.tutorId;
                    callback(null,null);
                }
            },
            function(data,callback){
                if(data){
                    if(data.length != tutorId.length){
                        var response = {};
                        response.message = Constants.responseMessage.TUTOR_NOT_FOUND;
                        response.data = {};
                        var errorResponse = {
                            response : response,
                            statusCode : 404
                        }
                        callback(errorResponse);
                    }
                }
                Dao.getData(Model.User,{_id: studentId,"courses.isStudying" : courseId},{},{lean : true, limit : 1},callback);              // check if student studies that course
            },
            function(data,callback){
                if(data.length == 0){
                    var response = {
                        message : Constants.responseMessage.COURSE_NOT_IN_PROFILE,
                        data : {}
                    }
                    var errorResponse = {
                        response : response,
                        statusCode : 400
                    }
                    callback(errorResponse);
                } else{
                    var insertData = {};
                    insertData.studentId = studentId;
                    insertData.courseId = courseId;
                    insertData.postedOn = Util.getUTCDate();
                    if(requestData.location) insertData.location = requestData.location;
                    if(requestData.startTime) insertData.startTime = requestData.startTime.toISOString();
                    if(requestData.duration && requestData.startTime){
                        var hoursToAdd = Math.floor(requestData.duration);
                        var minutesToAdd = Math.floor((requestData.duration - hoursToAdd)*60);
                        var endTime = new Date(requestData.startTime);
                        endTime.setUTCHours(endTime.getUTCHours() + hoursToAdd);
                        endTime.setUTCMinutes(endTime.getUTCMinutes() + minutesToAdd);
                        insertData.endTime = endTime.toISOString();
                    }
                    if(requestData.location) insertData.location = requestData.location;
                    if(requestData.requestType) insertData.requestType = requestData.requestType;
                    if(requestData.requestTime) insertData.requestTime = requestData.requestTime;
                    if(requestData.noOfStudents) insertData.noOfStudents = requestData.noOfStudents;
                    if(requestData.topics) insertData.topics = requestData.topics;
                    if(requestData.feeLower) insertData.feeLower = requestData.feeLower;
                    if(requestData.feeUpper) insertData.feeUpper = requestData.feeUpper;
                    if(requestData.description) insertData.description = requestData.description;
                    if(requestData.tutorId) insertData.tutorId = requestData.tutorId;
                    if(requestData.sessionType) insertData.sessionType = requestData.sessionType;

                    Dao.insertData(Model.RequestForTutor, insertData, callback);
                }
            },
            function(data,callback){
                var response = {
                    message : 'Requested posted successfully',
                    data : data
                }
                var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success);
            }

        ],
        function(err,result){
            if(err){
                callbackRoute(err)
            }
            else{
                callbackRoute(null, result);
            }
        }
    )
}

/*
 List Courses in which the student is enrolled
 */
exports.myCourses = function(requestData,callbackRoute){
    var studentId = requestData.id;
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var courses = data[0].courses.isStudying;
                Dao.getData(Model.Course,{_id : {$in : courses}},{code:1,title:1,topics:1,desc:1},{lean:true},callback);
            },
            function(data,callback){
                var response = {};
                var errorResponse = {
                    response : response
                };
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.response = response;
                    errorResponse.statusCode = 404;                                          // Not found Status Code
                    callback(errorResponse);
                } else {
                    response = {
                        message : 'Course details for user',
                        data : data
                    }
                    var success = {
                        response : response,
                        statusCode : 200

                    }
                    callback(null,success);
                }
            }
        ], function(err,success){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,success);
            }
        }
    )
}

/*
 Providing feedback to the tutor
 */

//exports.feedback = function(requestData,callbackRoute){
//    var studentId = requsetData.id;
//    var tutorId = requestData.tutorId;
//    var sessionId = requestData.sessionId;
//    Async.waterfall([
//            function(callback){
//                Validation.fieldValidation(Model.User,tutorId,{_id:1},{lean:true,limit:1},Constants.responseMessage.TUTOR_NOT_FOUND,callback);
//            },
//            function(data,callback){
//                Validation.fieldValidation(Model.Session,sessionId,{_id:1},{lean:true,limi:1},Constants.responseMessage.SESSION_NOT_FOUND,callback);
//            },
//            function(data,callback){
//                var feedback = {
//                    sessionId : sessionId,
//                    receiverId : tutorId,
//                    senderId : studentId,
//                    feedbackBody : requestData.feedbackBody,
//                    rating : requestData.rating
//                };
//                Dao.insertData(Model.Feedback,feedback,callback);
//                var response = {
//                    message : 'Feedback is successfully registered',
//                    data : {}
//                };
//                var result = {
//                    response :response,
//                    statusCode : 200
//                }
//                callback(null,result);
//            }
//        ],
//        function(err,result){
//            if(err){
//                callbackRoute(err);
//            } else {
//                callbackRoute(result);
//            }
//        }
//    )
//}

/*
 Mark a tutor as a favorite
 */

exports.markFavorite = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var tutorId = requestData.tutorId;
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var favoriteTutors = JSON.stringify(data[0].favorites.tutors);
                if(favoriteTutors.indexOf(tutorId) !=  -1){
                    var response = {
                        message : Constants.responseMessage.ALREADY_FAVORITE,
                        data : {}
                    }
                    var errorResponse = {
                        response : response,
                        statusCode : 400
                    }
                    callback(errorResponse);
                } else {
                    callback(null,null);
                }
            },
            function(data,callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.TUTOR_NOT_FOUND,callback);
            },
            function(data,callback){
                Dao.updateData(Model.User,{_id:studentId},{$push: {"favorites.tutors": tutorId}},{safe:true},callback);
            },
            function(data,callback){
                var response = {
                    message : 'Tutor marked as favorite',
                    data : {}
                }
                var result = {
                    response : response,
                    statusCode : 200
                }
                callback(null,result)
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        }
    )
}

/*
 Lists the sessions of a particular student
 */

exports.mySessions = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var query = {
        studentId : studentId
    };
    if(requestData.tutorId) query.tutorId = requestData.tutorId;
    if(requestData.status) query.status = requestData.status;
    if(requestData.courseId) query.courseId = requestData.courseId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var collectionOptions = {path : "courseId tutorId", select : "code title firstName lastName profilePictureThumb tutorRating", lean:true}
                Dao.getDataWithReference(Model.Session,query,{},collectionOptions,callback)
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 204;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else{
                callbackRoute(null,result);
            }
        })

}

/*
 Accept the modified requests / response to general requests by tutors
 */
//exports.acceptRequest = function(requestData,callbackRoute){
//    var studentId = requestData.id;
//    var requestId = requestData.requestId;
//    var response = {};
//    var errorResponse = {
//        response : response
//    }
//    var insertData = {};
//    Async.waterfall([
//            function(callback){
//                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback)
//            },
//            function(data,callback){
//                if(!requestId) console.log('Implementation Error')
//                Dao.getData(Model.RequestForStudent,{_id : requestId, studentId : studentId},{},{lean : true},callback);
//            },
//            function(data,callback){
//                if(data.length ==0){
//                    response.message = Constants.responseMessage.NO_DATA_FOUND;
//                    response.data = {};
//                    errorResponse.statusCode = 400;
//                    callback(errorResponse);
//                } else {
//                    if(data[0].studentId) insertData.studentId = data[0].studentId;
//                    if(data[0].tutorId) insertData.tutorId = data[0].tutorId;
//                    if(data[0].location) insertData.location = data[0].location;
//                    if(data[0].startTime) insertData.startTime = data[0].startTime;
//                    if(data[0].endTime) insertData.endTime = data[0].endTime;
//                    var parentRequestId = data[0].parentRequestId;
//                    Dao.getData(Model.RequestForTutor,{_id : parentRequestId},{},{lean : true, limit : 1},callback);
//                }
//            },
//            function(data,callback){
//                if(data.length ==0){
//                    response.message = Constants.responseMessage.NO_DATA_FOUND;
//                    response.data = {};
//                    errorResponse.statusCode = 400;
//                    callback(errorResponse);
//                } else {
//                    if(data[0].courseId) insertData.courseId = data[0].courseId;
//                    if(data[0].noOfStudents) insertData.noOfStudents = data[0].noOfStudents;
//                    if(data[0].topics) insertData.topics = data[0].topics;
//                    Dao.insertData(Model.Session,insertData,callback);
//                }
//            },
//            function(data,callback){
//                insertData = data;
//                Dao.deleteData(Model.RequestForStudent,{_id : requestId},callback);
//            },
//            function(data,callback){
//                Dao.updateData(Model.RequestForTutor,{_id : parentRequestId},{status : scheduled},{lean : true, limit : 1},callback);
//            },
//            function(data,callback){
//                if(data){
//                    response.message = Constants.responseMessage.SESSION_CREATED;
//                    response.data = insertData;
//                    var success = {
//                        response : response,
//                        statusCode : 201
//                    }
//                    callback(null,success);
//                }
//            }
//        ],
//        function(err,result){
//            if(err) callbackRoute(err);
//            else callbackRoute(null, result);
//        })
//}
/*
 Lists the status of current request posted by the user
 */
exports.myRequests = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    };
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var collectionOptions = {path : "courseId" , select : 'code title', lean : true}
                Dao.getDataWithReference(Model.RequestForTutor,{studentId:studentId},{},collectionOptions,callback);
            },
            function(data,callback){
                if(data==null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    };
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })
}

/*
 Lists the tutors who have taught/currently~teaching/scheduled~to~teach the student
 */

exports.myTutors = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    };
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{studentId :{$in : studentId }},{tutorId : 1},{lean: true},callback);
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.data = {};
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    errorResponse.statusCode = 404 ;
                    callback(errorResponse);
                } else {
                    callback(null,data);
                }
            },
            function(data,callback){
                var tutors = [];
                for (i = 0 ; i < data.length; i++) {
                    tutors.push(data[i].tutorId);
                }
                Dao.getData(Model.User,{_id:{$in : tutors}},{firstName:1, lastName:1,mobileNumber:1,college:1},{lean : true},callback);
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.data = {};
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    errorResponse.statusCode = 404 ;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    };
                    callback(null, success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })

}

/*
 *  Lists the payments made by the user
 */
exports.availableCourses = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var options = {lean : true};
    if(requestData.limit) options.limit = requestData.limit;
    if(requestData.offset) options.skip = requestData.offset;
    var response = {};
    var errorResponse = {
        response : response
    }
    var replyData = {};

    /** Async.waterfall([
     function(callback){
            Validation.fieldValidation(Model.User,{_id : studentId},{class : 1, year : 1},{lean : true, limit : 1},Constants.responseMessage.INVALID_ACCESS,callback);
        },
     function(data,callback){
            Dao.getData(Model.Class,{class : data[0].class, year : data[0].year},{courses : 1},{lean: true, limit:1},callback);
        },
     function(data,callback){
            if(data == null || data.length == 0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 404;
                callback(errorResponse);
            } else {
                Dao.getData(Model.Course,{_id : {$in : data[0].courses}},{title : 1 ,code: 1, topics : 1,desc : 1},{lean : true},callback);
            }
        },
     function(data,callback){
            response.message = Constants.responseMessage.DATA_FOUND;
            response.data = data;
            var success = {
                response : response,
                statusCode : 200
            }
            callback(null, success);
        }
     ],
     function(err,result){
        if(err){
            callbackRoute(err);
        } else {
            callbackRoute(null,result);
        }
    })
     **/                 // Code to check available courses based on the class and year (Depreciated)

    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Course,{ _id : {$nin : data[0].courses.isStudying}},{__v :0 , students : 0, tutors : 0},options,callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })

}

/*
 * Route to add the course in which the student is enrolled
 */

exports.addCourse = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var courseId = requestData.courseId;
    var response = {};
    var errorResponse = {
        response : response
    }
    var courseArray = new Array();
    Async.waterfall([
            function(callback){
                Dao.getData(Model.Course,{_id : {$in : courseId}},{},{lean : true}, callback);
            },
            function(data,callback){
                if(data.length != courseId.length){
                    response.message = Constants.responseMessage.COURSE_NOT_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                }
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var i = 0;
                var isStudying = JSON.stringify(data[0].courses.isStudying);
                for(i=0; i<courseId.length; i++){
                    if(isStudying.indexOf(courseId[i]) ==  -1){
                        courseArray.push(courseId[i]);
                    }
                }
                if(courseArray.length == 0){
                    response.message = Constants.responseMessage.COURSE_ALREADY_ADDED;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);

                } else if(data[0].courses.isStudying.length + courseArray.length > Constants.MAX_COURSES) {
                    response.message = Constants.responseMessage.MAX_COURSES_REACHED;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);

                } else {
                    Dao.updateData(Model.User,{_id : studentId},{$pushAll : {"courses.isStudying" : courseArray} },{},callback);
                }
            },
            function (data,callback) {
                if(data.n = 0){
                    response.message = Constants.responseMessage.NO_DATA;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.updateData(Model.Course,{_id : {$in : courseArray}},{$push : {students : studentId}},{multi : true},callback);
                }
            },
            function(data,callback){
                if(data.n = 0){
                    response.message = Constants.responseMessage.NO_DATA;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.COURSE_ADDED;
                    response.data = {};
                    var success = {
                        response : response,
                        statusCode : 201
                    }
                    callback(null, success)
                }

            }
        ],
        function(err,result){
            if(err ){
                callbackRoute(err);
            }  else {
                callbackRoute(null,result);
            }
        })
}


/*
 * Route to view the payments made by the student
 */
exports.myPayments = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    var replyData = {};
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Payment,{studentId : studentId},{studentId:1,tutorId:1,sessionId:1,paymentDate:1},{lean:true},callback);
            },
            function(data,callback){
                if(data==null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    replyData = data[0];
                    Dao.getData(Model.User, {_id: replyData.tutorId}, {firstName: 1, lastName: 1}, {
                        lean: true,
                        limit: 1
                    }, callback)
                }
            }
            ,function(data,callback) {
                replyData.tutorFirstName = data[0].firstName;
                replyData.tutorLastName = data[0].lastName;
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = replyData;
                var success = {
                    response: response,
                    statusCode: 200
                }
                callback(null, success);
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })
}


/****
 /*
 ----------------------------------------------------
 Approve a request from a tutor and create a session
 ----------------------------------------------------


 exports.approveRequest = function(requestData, callbackRoute){
 var studentId = requestData.id;
 var requestId = requestData.requestId;
 var response = {};
 var errorResponse = {
 response : response
 }
 Async.waterfall([
 function(callback){
 Dao.getData(Model.RequestForStudent,{_id : requestId,studentId : {$in : studentId}},{},{lean:true,limit:1},callback);
 },
 function(data,callback){
 if(data==null || data.length == 0){
 response.message = Constants.responseMessage.NO_DATA_FOUND;
 response.data = {};
 errorResponse.statusCode = 404;
 callback(errorResponse);
 } else {
 Dao.insertData(Model.session,{
 courseId:data.courseId,
 topics: data.topics,
 studentId : studentId,
 tutorId : data.tutorId,
 location : data.location,
 noOfStudents : data.noOfStudents,
 startTime : data.startTime,
 endtime : data.endTime,
 status : 'scheduled',
 sessionCost : data.price
 },callback);
 }
 },
 function(data,callback){

 }

 ])
 }
 */


/**
 * View the details of a particular tutor
 */

exports.viewTutor = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var query = {
                    "courses.canTeach" : {$elemMatch : {courseId : {$in: data[0].courses.isStudying}}}
                };
                if(requestData.courseId){
                    var isStudying = JSON.stringify(data[0].courses.isStudying);
                    if(isStudying.indexOf(requestData.courseId) == -1){
                        response.message = Constants.responseMessage.COURSE_NOT_FOUND;
                        response.data = {};
                        errorResponse.statusCode = 400;
                        callback(errorResponse);
                    } else {
                        query = {
                            "courses.canTeach" : {$elemMatch : {courseId : Mongoose.Types.ObjectId(requestData.courseId)}}
                        }
                    }
                }
                if(requestData.tutorId){
                    query._id = requestData.tutorId;
                }
                var projection = {firstName:1,
                    lastName:1,
                    rating:1,
                    profilePictureThumb :1,
                    majors :1,
                    college:1,
                    year :1,
                    fee:1,
                    tutorRating : 1,
                    bioDescription:1,
                    noOfCompletedSessions : 1,
                    "courses.canTeach" : 1};
                var collectionOptions = {path : "courses.canTeach.courseId" , select : "code title", lean : true}
                if(requestData.limit) collectionOptions.limit = requestData.limit;
                if(requestData.offset) collectionOptions.skip = requestData.offset;
                Dao.getDataWithReference(Model.User,query,projection,collectionOptions,callback);
            },
            function(data,callback){
                if(data==null || data.length==0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else{
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err)
            } else {
                callbackRoute(null,result);
            }
        })
}
/**
 * View the favorite tutors of student
 */
exports.myFavorites = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response :response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : studentId},{favorites:1},{lean:true,limit :1},callback);
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    var tutors = data[0].favorites.tutors;
                    callback(null,tutors);
                }
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : {$in : data}},{firstName : 1, lastName : 1, rating : 1,profilePictureThumb : 1},{lean:true},callback)
            },
            function(data,callback){
                if(data == null || data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })

}
/**
 * Add the card details of student
 */
exports.addCard = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                var savedCards = data[0].savedCards || [];
                if(requestData.stripeToken) var stripeToken = requestData.stripeToken;
                var customer_id = Stripe.customers.create({
                    source : stripeToken,
                    description : data[0].primaryEmail
                })
                if(!customer_id){
                    response.message = Constants.responseMessage.SERVER_ERROR;
                    response.data = {}
                    errorResponse.statusCode = 402;
                } else {
                    var newCard ={
                        lastFourDigits : requestData.lastFourDigits,
                        stripeCustomerId : customer_id
                    }
                    Dao.updateData(Model.User,{_id : studentId},{$push : {savedCards : newCard}},{limit:1,lean:true},callback);
                }
            }
        ],
        function(err, result){
            if(err){
                callbackRoute(err)
            } else {
                callbackRoute(result);
            }
        })
}
/**
 * Cancel a session
 */
exports.cancelSession = function(requestData,callbackRoute){
    var studentId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Dao.getData(Model.Session,{_id : sessionId, studentId : studentId,status:'scheduled'},{},{lean : true, limit :1},callback);
            },
            function(data,callback){
                if(data == null || data.length ==0){
                    response.message = Constants.responseMessage.INVALID_ACCESS;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.deleteData(Model.Session,{_id:sessionId},callback);
                }
            }
        ],
        function(err){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.SESSION_CANCELLED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,success);
            }
        })
}
/*
 * Reschedule a session
 */
exports.rescheduleSession = function(requestData,callbackRoute){
    var studentId = requestData.studentId;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Dao.getData(Model.Session,{_id :sessionId, studentId : studentId},{},{lean : true,limit : 1},callback)
            },
            function(data,callback){
                var startTime = requestData.startTime + Config.timeZone.adjust;
                var endTime = startTime;
                var hoursToAdd = Math.floor(requestData.duration);
                var minutesToAdd = Math.floor((requestData.duration-hoursToAdd)*60);
                endTime.setUTCHours(endTime.getUTCHours()+hoursToAdd);
                endTime.setUTCMinutes(endTime.getUTCMinutes()+minutesToAdd);
                var updateData = {
                    startTime : startTime,
                    endTime : endTime
                }
                Dao.updateData(Model.Session,{_id: sessionId},updateData,{},callback)
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else {
                response.message = Constants.responseMessage.SESSION_RESCHEDULED;
                response.data = {};
                var result = {
                    response : response,
                    statusCode : 201
                }
                callbackRoute(null,result);
            }
        })
}
/**
 * Extend an ongoing session
 */
exports.extendSession = function(requestData , callbackRoute) {
    var studentId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId , status : 'extended'},{},{lean : true , limit : 1},callback);
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    var extendTime = data[0].extendTime;
                    var hoursToAdd = Math.floor(extendTime);
                    var minutesToAdd = Math.floor((extendTime - hoursToAdd)*60);
                    var endTime = data[0].endTime;
                    endTime.setUTCHours(endTime.getUTCHours() + hoursToAdd);
                    endTime.setUTCMinutes(endTime.getUTCMinutes() + minutesToAdd);

                    Dao.updateData(Model.Session,{_id : sessionId},{endTime : endTime, status : "scheduled"},{},callback);
                }
            },
            function(data,callback){
                if(data.n != 0){
                    response.message = Constants.responseMessage.SESSION_EXTENDED;
                    response.data = {};
                    var success = {
                        response : response,
                        statusCode : 201
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}
/**
 * View availability of a tutor
 */
exports.viewAvailability = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var tutorId = requestData.tutorId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Validation.roleValidation(tutorId,'tutor',Constants.responseMessage.TUTOR_NOT_FOUND,callback);
            },
            function(data,callback){
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = {
                    availability : data[0].availability,
                    firstName : data[0].firstName,
                    lastName : data[0].lastName,
                    profilePicture : data[0].profilePicture,
                    tutorRating : data[0].tutorRating
                };
                var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(result);
        })
}
/**
 * Rate the tutor after a session
 */
exports.rateTutor = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                Dao.getData(Model.Session,{_id : sessionId},{tutorId : 1, ratingByStudent : 1},{lean : true, limit : 1},callback)
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else if(data[0].ratingByStudent != []){
                    response.message = Constants.responseMessage.SESSION_ALREADY_RATED;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    Dao.getData(Model.User,{_id : data[0].tutorId},{tutorRating : 1},{lean : true, limit : 1},callback);
                }
            },
            function(data,callback){
                if(data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    var ratingToAdd = (requestData.rating[0] + requestData.rating[1] + requestData.rating[2] + requestData.rating[3])/4;
                    var tutorRating = (data[0].tutorRating + ratingToAdd) /2;
                    Dao.updateData(Model.User,{_id : data[0]._id},{tutorRating : tutorRating},{limit : 1},callback);
                }
            },
            function(data,callback){
                Dao.updateData(Model.Session,{_id : sessionId},{ratingByStudent : requestData.rating},{limit : 1},callback);
            },
            function(data,callback){
                response.message =  Constants.responseMessage.SESSION_RATED;
                response.data = {}
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err) callbackRoute(err);
            else callbackRoute(null,result);
        })
}
/**
 * View the details of a particular request
 */
exports.viewRequest = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var requestId = requestData.requestId;
    var response = {}
    var errorResponse = {
        response : response
    }
    var replyData = {}
    Async.waterfall([
        function(callback){
            Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            var collectionOptions = {path : "courseId tutorId", select : "code title firstName lastName profilePictureThumb", lean : true, limit : 1}
            Dao.getDataWithReference(Model.RequestForTutor, {_id : requestId, studentId : studentId },{},collectionOptions, callback);
        },
        function(data,callback){
            if(data.length == 0 ){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 404;
                callback(errorResponse);
            } else if(data[0].noOfInterests == 0){
               replyData = data[0];
                callback(null,null);
            } else {

                if(data[0].requestType == 'personal') {
                    replyData = data[0];
                    if(data[0].fee) replyData.tutorId.fee = data[0].fee;
                    if(replyData.fee) delete replyData.fee;
                    callback(null,null);

                } else if(data[0].requestType == 'general'){
                    replyData = data[0];
                    Dao.getData(Model.RequestForStudent,{parentRequestId : requestId},{fee : 1, tutorId : 1, _id : 0},{lean : true},callback);
                }
            }
        },
        function(data,callback){
                if(data && data.length == 0){
                   var tutorArray = replyData.tutorId;
                   for(var i=0;i < tutorArray.length; i++){
                      var tutorFee =  _.filter(data,{tutorId : tutorId})[0].fee;
                      tutorArray.fee = tutorFee;
                   }
                   replyData.tutorId = tutorArray;
                   callback(null,null);
                } else {
                    callback(null,null);
                }
        },
        function(data,callback){
            response.message = Constants.responseMessage.DATA_FOUND;
            response.data = replyData;
            var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success);
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null, result);
    })
}
/**
 *  Accept a request and schedule session
 */
exports.acceptRequest = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var requestId = requestData.requestId;
    if(requestData.tutorId) var tutorId = requestData.tutorId;
    var insertData = {};
    var replyData;
    var response = {};
    var errorResponse = {
        response : response
    }
    var courseId;           // Store the courseId of the current request
    Async.waterfall([
        function(callback){
            Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            Dao.getData(Model.RequestForTutor,{_id : requestId, status : {$in : ['pending','accepted','modified']}},{},{lean : true, limit : 1},callback);
        },
        function(data,callback){
            if(data.length == 0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 404;
                callback(errorResponse);
            } else {
                courseId = data[0].courseId;
                insertData.courseId = data[0].courseId;
                if(data[0].topics) insertData.topics = data[0].topics;
                insertData.studentId = data[0].studentId;
                insertData.location = data[0].location;
                insertData.sessionType = data[0].sessionType;
                insertData.status = 'scheduled';
                if(data[0].requestTime == 'scheduled'){
                    insertData.startTime = data[0].startTime;                                       // Set session time according to the time given in the request
                    insertData.endTime = data[0].endTime;
                } else {
                    var currentTime = new Date();                                                   // Set session time to cuurent time + some given duration
                    var durationToStartSession = Constants.DURATION_TO_START_SESSION                // Time after which onDemand session will be started
                    var hoursToAdd = Math.floor(durationToStartSession);
                    var minutesToAdd = Math.floor((durationToStartSession - hoursToAdd)*60);
                    currentTime.setUTCHours(currentTime.getUTCHours() + hoursToAdd);
                    currentTime.setUTCMinutes(currentTime.getUTCMinutes() + minutesToAdd);
                    insertData.startTime = currentTime.toISOString();
                    var endTime = new Date(currentTime);
                    //var sessionDuration = durationToStartSession + data[0].duration;
                    hoursToAdd = Math.floor(durationToStartSession);
                    minutesToAdd = Math.floor((durationToStartSession - hoursToAdd)*60);
                    endTime.setUTCHours(endTime.getUTCHours() + hoursToAdd);
                    endTime.setUTCMinutes(endTime.getUTCMinutes() + minutesToAdd);
                    insertData.endTime = currentTime.toISOString();
                }
                if(data[0].requestType == 'personal'){
                   if(data[0].status == 'accepted' && data[0].tutorId.length == 1){
                       insertData.tutorId = data[0].tutorId[0];
                       Dao.getData(Model.User,{_id : data[0].tutorId[0]},{"courses.canTeach": 1},{lean : true, limit : 1},callback); // Get the tutor fee from the database
                   } else {
                       response.message = Constants.responseMessage.REQUEST_NOT_ACCEPTED;
                       response.data = {};
                       errorResponse.statusCode = 400;
                       callback(errorResponse);
                   }
                } else if(data[0].requestType == 'general' && tutorId){
                    //Dao.getData(Model.RequestForStudent,{parentRequestId : requestId, tutorId : tutorId},{tutorId : 1,fee : 1},{lean : true, limit : 1},callback);
                    // Code changed get fee from the tutor profile where he has set fee for the individual course
                    insertData.tutorId = tutorId;
                    Dao.getData(Model.User,{_id : tutorId},{"courses.canTeach" : 1},{lean : true, limit : 1},callback);   // Get tutor fee from the database corresponding to courseId
                } else {
                    response.message = Constants.responseMessage.PARAMETER_MISSING;
                    response.data = {};
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                }
            }
        },
        function(data,callback){
            if(data.length == 0){
                response.message = Constants.responseMessage.TUTOR_NOT_FOUND;
                response.data = {};
                errorResponse.statusCode = 204;
                callback(errorResponse);
            } else {
                var courseFee = _.filter(data[0].courses.canTeach,function(object){
                    if(String(object.courseId) == String(courseId)){
                        return true;
                    } else {
                        return false;
                    }
                })[0].courseFee;
                callback(null,courseFee);
            }
        },
        function(data,callback){
            insertData.sessionCost = data;
            Dao.insertData(Model.Session,insertData,callback);
        },
        function(data,callback) {
            replyData = data;
            Dao.updateData(Model.RequestForTutor, {_id: requestId}, {status: 'scheduled'}, {}, callback);          // Change request status to scheduled
        },
        function(data,callback){
            response.message = Constants.responseMessage.SESSION_CREATED;
            response.data = replyData;
            var success = {
                response : response,
                statusCode : 201
            }
            // Code to schedule session and payment related
            // and push notification
            // insert here
            Schedule.scheduleSessionStart(replyData._id,replyData.startTime);
            Schedule.scheduleSessionEnd(replyData._id,replyData.endTime);
            //Schedule related tasks above
            // Schedule push notification above
            callback(null,success);
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null,result);
    })
}
/**
 * Schedule a completed session again
 */
exports.scheduleAgain = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {}
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            Dao.getData(Model.Session,{_id : sessionId, studentId : studentId, status : "completed"},{_id : 0, __v : 0,studentRating : 0,tutorRating : 0,extendTime : 0},{lean : true, limit : 1},callback);
        },
        function(data,callback){
            if(data.length == 0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 204;
                callback(errorResponse);
            } else {
                var insertData = data[0];
                if(requestData.location) insertData.location = requestData.location;
                if(requestData.startTime) insertData.startTime = requestData.startTime.toISOString();
                if(requestData.duration){
                    var hoursToAdd = Math.floor(requestData.duration);
                    var minutesToAdd = Math.floor((requestData.duration - hoursToAdd)*60);
                    var endTime = new Date(requestData.startTime);
                    endTime.setUTCHours(endTime.getUTCHours() + hoursToAdd);
                    endTime.setUTCMinutes(endTime.getUTCMinutes() + minutesToAdd);
                    insertData.endTime = endTime.toISOString();
                }
                insertData.status = 'pending';
                Dao.insertData(Model.Session,insertData,callback);
            }
        },
        function(data,callback){
            // Send push notification for confirmation
            response.message = Constants.responseMessage.REQUEST_SENT;
            response.data = {}
            var success = {
                response : response,
                statusCode : 201
            }
            callback(null,success);
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null,result);
    })

}
/**
 * Confirm reschedule requested by the tutor
 */
exports.confirmReschedule = function(requestData, callbackRoute){
    var studentId = requestData.id;
    var sessionId = requestData.sessionId;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            Dao.getData(Model.Session,{_id : sessionId, status : "pending", studentId : studentId},{},{limit : 1},callback);
        },
        function(data,callback){
            if(data.length == 0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 404;
                callback(errorResponse);
            } else {
                if(requestData.response="accept"){
                    Dao.updateData(Model.Session,{_id : sessionId},{status : "scheduled"},{limit : 1},callback);
                } else {
                    Dao.deleteData(Model.Session,{_id : sessionId},callback);
                }
            }
        },
        function(da2ata,callback){
            if(data.nRemoved && data.nRemoved > 0){
                response.message = Constants.responseMessage.SESSION_CANCELLED;
                response.data = {}
            } else {
                response.message = Constants.responseMessage.SESSION_RESCHEDULED;
                response.data ={}
            }
            var success ={
                response : response,
                statusCode : 201
            }
            callback(null,success);
        }
    ],
    function(err,result){
        if(err){
            callbackRoute(err)
        } else {
            callbackRoute(null,result);
        }
    })
}
/**
 * Confirm schedule again by student
 */
exports.confirmScheduleAgain = function(requestData, callbackRoute){
   var studentId = requestData.id;
   var sessionId = requestData.sessionid;
   var response = {}
   var errorResponse = {
       response : response
   }
   Async.waterfall([
       function(callback){
           Validation.roleValidation(studentId,'student',Constants.responseMessage.INVALID_ACCESS,callback);
       },
       function(data,callback){
           Dao.getData(Model.Session,{_id : sessionId, studentId : studentId, status : "pending"},{},{lean : true, limit : 1},callback);
       },
       function(data,callback){
           if(data.length == 0){
               response.message = Constants.responseMessage.NO_DATA_FOUND;
               response.data = {}
               errorResponse.statusCode = 204;
               callback(errorResponse);
           } else {
               // Schedule sessions with Agenda module
               // *********
               // update status to scheduled
               Dao.updateData(Model.Session,{_id : sessionId},{status : "scheduled"},{limit : 1},callback);
           }
       },
       function(data,callback){
           response.message = Constants.responseMessage.SESSION_CREATED;
           response.data = {}
           var success = {
               response : response,
               statusCode : 201
               }
           callback(null,success);
       }
   ],
   function(err,result){
       if(err) callbackRoute(err);
       else callbackRoute(null,result);
   })
}