/**
 * Created by Vikas Sharma on 7/7/15.
 */

var Constants = require('../utilities/constants');
var Model = require('../models');
var Dao = require('../dao/DAO');
var Async = require('async');
var Util = require('../utilities/util');
var Validation = require('../utilities/validationFunctions');
var Logger = require('../utilities/logger');
/**
 ******************************************
 Show the details of sessions to the admin
 ******************************************
 */

exports.showSessions = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var query = {};
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                if(requestData.sessionStatus) query.status = requestData.sessionStatus;
                if(requestData.sessionId)   query._id = requestData.sessionId;
                Dao.getData(Model.Session,query,{},{lean:true},callback);
            },
            function(data,callback){
                if(data == null ||data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                    Logger.info('No data found in the database. Bad request')
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                Logger.warn("There was a error in processing request",err.response.message);
                callbackRoute(err);
            } else {
                Logger.info("Showing sessions to the user");
                callbackRoute(null,result);
            }
        })
}

/**
 Show the details of sessions to the admin
 */

exports.showPayments = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var query = {};
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                if(requestData.paymentId)  query._id = requestData.paymentId;
                if(requestData.studentId)  query.studentId = requestData.studentId;
                if(requestData.tutorId)    query.tutorId = requestData.tutorId;
                Dao.getData(Model.Session, query,{},{lean:true},callback);
            },
            function(data,callback){
                if(data == null ||data.length == 0){
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    response.data = {};
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    response.message = Constants.responseMessage.DATA_FOUND;
                    response.data = data;
                    var success = {
                        response : response,
                        statusCode : 200
                    }
                    callback(null,success);
                }
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })
}
/*
 *****************************************
 Show the details of a user
 *****************************************
 */

exports.showUser = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                if(requestData.userId){
                    var userId = requestData.userId;
                    Validation.fieldValidation(Model.User,userId,{password:0},{lean:true,limit:1},Constants.responseMessage.USER_NOT_FOUND,callback);
                } else {
                    var options = {lean : true};
                    if(requestData.limit) options.limit = requestData.limit;
                    if(requestData.offset) options.skip = requestData.offset;
                    var query = {_id : {$nin : [adminId]}}
                    if(requestData.role) query.roles = requestData.role;
                    Dao.getData(Model.User,query,{password : 0},options,callback);
                }
            },
            function(data,callback){
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = data;
                var success = {
                    response: response,
                    statusCode : 200
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err){
                callbackRoute(err);
            } else {
                callbackRoute(null,result);
            }
        })
}

/*
 =========================================================
 Show the details of feedback given  by students or tutors
 =========================================================
 */
exports.showFeedback = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var query = {};
    var replyData={};
    var receiverId = requestData.receiverId;
    var senderId = reuestData.senderId;
    var response ={};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
            function(callback){
                Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
            },
            function(data,callback){
                if(receiverId) {
                    Validation.fieldValidation(Model.User,receiverId,{},{limit:1},Constants.responseMessage.NO_DATA_FOUND,callback);
                    query.receiverId = receiverId;
                }
                else callback();
            },
            function(data,callback){
                if(senderId) {
                    query.senderId = senderId;
                    Validation.fieldValidation(Model.User,senderId,{},{limit:1},Constants.responseMessage.NO_DATA_FOUND,callback);
                }
                else callback();
            },
            function(data,callback){
                Dao.getData(Model.Feedback,query,{},{lean:true},callback);
            },
            function(data,callback){
                if(data.length == 0 || data == null){
                    response.data = {};
                    response.message = Constants.responseMessage.NO_DATA_FOUND;
                    errorResponse.statusCode = 404;
                    callback(errorResponse);
                } else {
                    replyData = data;
                    Dao.getData(Model.User, {_id: {$in: data.senderId}}, {
                        firstName: 1,
                        lastName: 1
                    }, {lean: true}, callback);
                }
            },
            function(data,callback){
                for(i=0;i<data.length;i++) {
                    replyData[i].senderName = data[i].firstName + ' ' + data[i].lastName;
                }
                callback();
            },
            function(data,callback){
                Dao.getData(Model.User,{_id : {$in : data.receiverId}},{firstName:1,lastName : 1},{lean: true},callback);
            },
            function(data,callback){
                for(i=0;i<data.length;i++){
                    replyData[i].receiverName = data[i].firstName + ' ' + data[i].lastName;
                }
                callback();
            },
            function(data,callback){
                response.data = replyData;
                response.message = 'Data Found';
                var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success);
            }
        ],
        function(err,result){
            if(err){
                callback(err);
            } else {
                callback(null,result);
            }
        })
}

/**
 * Show the transcript of a tutor to the admin so that he can approve tutor
 */
exports.approveTutor = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var tutorId = requestData.tutorId;
    var response = {};
    var errorResponse = {
        response : response
    }
    var success = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            Dao.getData(Model.User,{_id : tutorId, roles : 'tutor'},{isApproved : 1},{limit:1,lean : true},callback);
        },
        function (data,callback) {
            if (data.length == 0) {
                response.message = Constants.responseMessage.TUTOR_NOT_FOUND;
                response.data = {};
                errorResponse.statusCode = 400;
                callback(errorResponse);

            } else if (data[0].isApprovedTutor == true) {
                response.message = Constants.responseMessage.TUTOR_ALREADY_APPROVED;
                response.data = {};
                errorResponse.statusCode = 400;
                callback(errorResponse);

            } else {
                Dao.updateData(Model.User, {_id: tutorId}, {isApprovedTutor: true}, {}, callback)
            }
        },
            function(data,callback) {
                response.message = Constants.responseMessage.TUTOR_APPROVED;
                response.data = {};
                success.statusCode = 201;
                callback(null, success);
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null,result);
    })
}

/**
 * Add the classes so that users can add the courses they are enrolled in
*/

exports.addCourse = function(requestData,callbackRoute){
    var adminId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Validation.roleValidation(adminId,'admin',Constants.responseMessage.INVALID_ACCESS,callback);
        },
        function(data,callback){
            var insertData = {};
            if(requestData.courseCode) insertData.code = requestData.courseCode;
            if(requestData.courseTitle) insertData.title = requestData.courseTitle;
            if(requestData.topics) insertData.topics = requestData.topics;
            if(requestData.desc) insertData.desc = requestData.desc;
            if(insertData != null){
                insertData.tutors = new Array();
                insertData.students = new Array();
                Dao.insertData(Model.Course,insertData,callback);
            } else {
                response.message = Constants.responseMessage.PARAMETER_MISSING;
                response.data = {};
                errorResponse.statusCode = 400;
                callback(errorResponse);
            }
        },
        function(data,callback){
            response.message = Constants.responseMessage.COURSE_ADDED;
            response.data = data;
            var success = {
                response : response,
                statusCode : 201
                }
            callback(null,success);
        }
    ],
    function(err,result){
        if(err){
            if (err.details.message.indexOf("duplicate") != -1) {
                err.response = {
                    message : Constants.responseMessage.COURSE_ALREADY_EXIST,
                    data : {}
                }
             err.statusCode = 400;
            }
          callbackRoute(err);
        } else {
            callbackRoute(null,result);
        }
    })
}