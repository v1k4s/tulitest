/**
 * Created by Vikas Sharma on 7/8/15.
 */

var userController = require('./userController');
var adminController = require('./adminController');
var studentController = require('./studentController');
var tutorController = require('./tutorController');

module.exports = {
    userController : userController,
    adminController : adminController,
    studentController : studentController,
    tutorController : tutorController
}