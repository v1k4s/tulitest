/**
 * Created by Vikas Sharma on 7/3/15.
 */

var Constants = require('../utilities/constants');
var Model = require('../models');
var Dao = require('../dao/DAO');
var Async = require('async');
var Util = require('../utilities/util');
var TokenFunctions = require('../utilities/tokenFunctions')
var Jwt = require('jsonwebtoken');
var Config = require('../config');
var Validation   = require('../utilities/validationFunctions');
var FileUtilities = require('../utilities/fileUtilities');
var Hapi = require('hapi');
var Logger = require('../utilities/logger');
var _ = require("lodash");
var Stripe = require('stripe')(Config.stripe.testKey);
/*
 --------------------------------------
 Login controller to handle the login
 --------------------------------------
 */

exports.login = function(userData,callbackRoute){
    var userId;
    var replyData = {};
    Logger.debug(userData);
    Async.waterfall([
            function(callback){                                                             // Ist function to handle task of getting data from database
                userData.lastLogin = Util.getUTCDate();
                if(userData.fbLoginFlag == 0) {
                    userData.password = Util.calculateHash('sha256', userData.password);
                    var query = {};
                    if (Util.validateEmail(userData.email)) {
                        query.$or = [{primaryEmail: userData.email}, {facebookEmail: userData.email}];
                    } else {
                        var response = {};
                        var errorResponse = {
                            response: response
                        }
                        response.message = Constants.responseMessage.INVALID_EMAIL_ID;
                        response.data = {};
                        errorResponse.statusCode = 400;
                        callback(errorResponse);
                    }
                } else {
                    var query = {};
                    query.fbId = userData.fbId;
                }
                var projection = {_id:1, password:1, isVerified : 1, firstName : 1, lastName : 1, profilePicture : 1, profilePictureThumb : 1, roles : 1};
                var options = {lean:true, limit : 1};
                Dao.getData(Model.User,query,projection,options,callback);                              // callback gives data to next function in the array
            },
            function(data,callback){                                                                   // 2nd function to check data and frame error response. Data is passed from previous function
                var response = {};
                var errorResponse = {
                    response : response
                };
                if(data==null || data.length==0){
                    response.message = Constants.responseMessage.USER_NOT_FOUND;
                    response.data = {};
                    errorResponse.response = response;
                    errorResponse.statusCode = 404;                                          // Not found Status Code
                    callback(errorResponse);                                                 // Error occurred and pass on the control to last function
                } else if (userData.fbLoginFlag == 0 && userData.password != data[0].password){
                    response.message = Constants.responseMessage.WRONG_PASSWORD;
                    response.data = {};
                    errorResponse.response = response;
                    errorResponse.statusCode = 401;                                          // Not authorized status code
                    callback(errorResponse);                                                 //Error occurred and pass on the control to last function
                } else if(data[0].isVerified == false){
                    response.message = Constants.responseMessage.ACCOUNT_NOT_VERIFIED;
                    response.data = {};
                    errorResponse.response = response;
                    errorResponse.statusCode = 401;
                    callback(errorResponse);
                } else {
                    replyData.profilePicture = data[0].profilePicture;
                    replyData.profilePictureThumb = data[0].profilePictureThumb;
                    replyData.firstName = data[0].firstName;
                    replyData.lastName = data[0].lastName;
                    replyData.userId = data[0]._id;
                    if(data[0].roles == 'student' && data[0].roles == 'tutor'){
                        replyData.role = 'both'
                    } else if(data[0].roles == 'student'){
                        replyData.role = 'student'
                    } else if(data[0].roles == 'tutor'){
                        replyData.role = 'tutor'
                    } else if(data[0].roles == 'admin'){
                        replyData.role = 'admin'
                    }
                    var tokenData = {                                                        // Store data and create a token out of it
                        id : data[0]._id,
                        lastLogin : userData.lastLogin,
                        message : 'Login Access Token'
                    };
                    var accessToken = Jwt.sign(tokenData,Config.secret.key);                      // Create a token out of tokenData
                    var updateData = {                                                       // Store the updated token, login and device details in database
                        lastLogin : userData.lastLogin
                    }
                    if(userData.deviceType) updateData.deviceType = userData.deviceType.toLowerCase()
                    if(userData.appVersion) updateData.appVersion = userData.appVersion;
                    if(userData.deviceToken) updateData.deviceToken = userData.deviceToken;
                    userId = data[0]._id;
                    TokenFunctions.createToken(userId,accessToken);                           // Set the access token in redis
                    replyData.accessToken = accessToken;                                     // Send the accessToken so that user can send it in subsequent requests
                    Dao.updateData(Model.User,{_id:userId},updateData,{lean:true,limit:1},callback);     // Update the database and return the Data to next function in array
                }
            },
            function(data,callback){                                                         // Frame the data to be sent
                callback(null,replyData);                                                    // Call the final function with no error and reply object
            }
        ],function(err,result){                                                              // final function to be called
            if(err) callbackRoute(err);                                                      // Callback function with errorResponse object
            else {
                var response = {
                    message : Constants.responseMessage.LOGIN_SUCCESSFULLY,                  // Frame the response
                    data : result
                }
                var success = {
                    response : response,
                    statusCode : 200
                }
                callbackRoute(null,success);                                                 // Send the reponse with error->null
            }

        }
    )
}

/*
 --------------------------------------
 User forgets the password
 --------------------------------------
 */

exports.forgotPassword = function(userData,callbackRoute){
    var userId  = requestData.id;
    var response = {};
    var errorResponse = {
        response: response
    }
    Logger.debug(userData);
    Async.waterfall([
        function(callback){
            Validation.fieldValidation(Model.User,studentId,{accessToken:1},{lean:true, limit:1},Constants.responseMessage.STUDENT_NOT_FOUND);
        },
        function(data,callback){

        }
    ])
}
/*
 ----------------
 Verify the user
 ----------------
 */
exports.verify = function(requestData,callbackRoute) {
    var tokenData = requestData.tokenData;
    var response = {};
    var errorResponse = {
        response: response
    }
    Logger.debug(requestData);
    Async.waterfall([
            function (callback) {
                Jwt.verify(tokenData, Config.secret.key, function (err, data) {
                    if (err || data.message != 'Verify User') {
                        response.data = {};
                        response.message = Constants.responseMessage.INVALID_ACCESS_TOKEN;
                        errorResponse.statusCode = 400;
                        callback(errorResponse);
                    } else {
                        callback(null, data);
                    }
                });
            },
            function (data, callback) {
                Dao.getData(Model.User, {_id: data.id}, {_id : 0,firstName : 1, lastName : 1, isVerified : 1}, {lean: true, limit: 1}, callback)
            },
            function (data, callback) {
                if (data == null || data.length == 0) {
                    response.data = {};
                    response.message = Constants.responseMessage.INVALID_ACCESS_TOKEN;
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else if(data[0].isVerified == true){
                    response.data = {};
                    response.message = Constants.responseMessage.ACCOUNT_ALREADY_VERIFIED;
                    errorResponse.statusCode = 400;
                    callback(errorResponse);
                } else {
                    var updateData = data[0];
                    updateData.isVerified = true;
                    Dao.updateData(Model.User, {_id: data[0]._id}, updateData, {lean: true, limit:1}, callback)
                }
            },
            function (data, callback) {
                response.message = Constants.responseMessage.VERIFICATION_SUCCESSFUL;
                response.data = {firstName: data.firstName, lastName: data.lastName}
                var success = {
                    response: response,
                    statusCode: 200
                }
                callback(null, success);
            }
        ],
        function (err, result) {
            if (err) {
                callbackRoute(err);
            } else {
                callbackRoute(null, result);
            }
        })
}

/**
 * Function to handle user registration
 */
exports.signUp = function(requestData,callbackRoute){
    var response = {};
    var errorResponse = {
        response : response
    }
    Logger.debug(requestData);
    var urls = {
        profilePicture: Config.s3BucketCredentials.defaultPicUrl,
        profilePictureThumb: Config.s3BucketCredentials.defaultPicUrl
    }
    console.log(requestData.courses[0]);
    if(requestData.roles.indexOf('tutor') != -1){
      if(requestData.majors == null  //&& requestData.majors.length == 0
          || requestData.minors == null //&& requestData.minors.length == 0)
          || requestData.bioDescription==null
          //|| requestData.availability == null   // make it optional for website development
          || requestData.courses == null){
          response.message = Constants.responseMessage.ACCOUNT_NOT_REGISTERED_PROPERLY;
          response.data = {};
          errorResponse.statusCode = 400;
          return callbackRoute(errorResponse);
      }
    }
        Async.waterfall([
                function (callback) {
                    if (requestData.roles.indexOf('tutor') != -1) {
                        if (requestData.transcript && requestData.transcript[0].hapi && requestData.transcript[0].hapi.filename) {
                            Logger.info('Trying to upload Transcripts');
                            FileUtilities.uploadTranscripts(requestData.transcript, requestData.mobileNumber, callback);
                        }
                        //} else {
                        //    response.message = Constants.responseMessage.ACCOUNT_NOT_REGISTERED_PROPERLY;
                        //    response.data = {};
                        //    errorResponse.statusCode = 400;
                        //    callback(errorResponse);
                        //}
                        callback(null,null);
                    } else {
                        if (requestData.fee) delete requestData.fee;
                        if (requestData.transcripts) delete requestData.transcripts;
                        callback(null, null);
                    }
                },
                function (data, callback) {
                    if (data) {
                        requestData.transcripts = data;
                    } else {
                        requestData.transcripts = [];
                    }
                    if (requestData.fbId && requestData.fbId != '') {
                        Logger.info("Getting Profile Pictures from Facebook")
                        urls.profilePicture = Constants.fbURL + requestData.fbId + "/picture?width=160&height=160";
                        urls.profilePictureThumb = Constants.fbURL + requestData.fbId + "/picture?width=160&height=160";
                        callback(null, urls);
                    } else if (requestData.profilePicture && requestData.profilePicture.hapi && requestData.profilePicture.hapi.filename) {
                        Logger.info("Uploading profile Picture");
                        FileUtilities.uploadProfilePicture(requestData.profilePicture, requestData.mobileNumber, callback);
                    } else {
                        callback(null, urls);
                    }
                },
                function (data, callback) {
                    var date = new Date();
                    if(requestData.password){
                        var hash = Util.calculateHash('sha256', requestData.password);
                        requestData.password = hash;
                    }
                    requestData.registrationDate = Util.getUTCDate();
                    requestData.profilePicture = data.profilePicture;
                    requestData.profilePictureThumb = data.profilePictureThumb;
                    if (requestData.courses) {
                        if (requestData.roles.indexOf('tutor') != -1) {
                            var courses = requestData.courses;
                            delete requestData.courses;
                            requestData.courses = {}
                            requestData.courses.canTeach = courses;
                        } else {
                            var courses = requestData.courses;
                            delete requestData.courses;
                            requestData.courses = {};
                            requestData.courses.isStudying = _.pluck(courses,'courseId');
                        }
                    }
                    if(requestData.fbRegisterFlag == 1){
                        if(requestData.password) delete requestData.password;
                    } else {
                        delete requestData.fbId;
                        delete requestData.facebookEmail;
                    }
                    if(requestData.stripeToken && requestData.stripeToken != ''){
                        requestData.savedCards = []
                        Stripe.customers.create({
                            description: 'Customer for ' + requestData.primaryEmail,
                            source: requestData.stripeToken
                        }, function(err, customer) {
                            if(err){
                                response.message = Constants.responseMessage.INVALID_STRIPE_TOKEN;
                                response.data = {}
                                errorResponse.statusCode = 400
                                callback(errorResponse);
                            } else {
                                requestData.savedCards.push(customer.id);
                                delete requestData.stripeToken;
                            }
                        });
                    }
                    callback(null,null);
                },
                function(data,callback){
                    Dao.insertData(Model.User, requestData, callback)
                },
                function (data, callback) {
                    response.message = Constants.responseMessage.REGISTRATION_SUCCESSFUL;
                    response.data = {
                        id: data._id,
                        email: data.primaryEmail
                    }
                    var success = {
                        response: response,
                        statusCode: 201
                    }
                    callback(null, success);
                }
            ],
            function (err, result) {
                if (err) {
                    if (err.details) {
                        if (err.details.message.indexOf("duplicate") != -1) {
                            if ((err.details.message.indexOf("primaryEmail") != -1) || (err.details.message.indexOf("facebookEmail") != -1)) {
                                err.response = {
                                    message: Constants.responseMessage.EMAIL_ALREADY_EXISTS,
                                    data: {}
                                }
                            }
                            //else if (err.details.message.indexOf("userName") != -1) {
                            //    err.response = {
                            //        message: Constants.responseMessage.USERNAME_ALREADY_EXISTS,
                            //        data: {}
                            //    }
                            //}
                            else if (err.details.message.indexOf("mobileNumber") != -1) {
                                err.response = {
                                    message: Constants.responseMessage.MOBILE_ALREADY_EXISTS,
                                    data: {}
                                }
                            }
                            err.statusCode = 409;
                        }
                    }
                    callbackRoute(err);
                } else {
                    callbackRoute(null, result);
                }
            })
}

/*
*Function to log out the user
*/
exports.logOut = function(requestData,callbackRoute){
    var response = {};
    var errorResponse = {
        response : response
    }
    console.log(requestData.token);
    if(requestData.token){
        TokenFunctions.expireToken(requestData.token,function(err,data){
            if(err){
                response.message = Constants.responseMessage.SESSION_EXPIRED;
                response.data = {};
                errorResponse.statusCode = 401;
                callbackRoute(errorResponse);
            } else {
                response.message = Constants.responseMessage.LOGOUT_SUCCESSFULLY;
                response.data = {};
                var success = {
                    response : response
                }
                success.statusCode = 201;
                callbackRoute(null, success);
            }
        });

    } else {
        response.message = Constants.responseMessage.SESSION_EXPIRED;
        response.data = {};
        errorResponse.statusCode = 401;
        callbackRoute(errorResponse);
    }
}

///**
// * Function to add card details to the database
// */
////exports.addCard = function(requestData,callbackRoute){
////    var userId = requestData.id;
////    var response = {};
////    var errorResponse = {
////        response : response
////    }
////    var stripeToken = requestData.stripeToken;
////    var lastFourDigits = requestData.lastFourDigits;
////    Async.waterfall([
////        function (callback) {
////
////        }
////    ])
////}

/**
 * View my profile
 */
exports.myProfile = function(requestData,callbackRoute){
    var userId = requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Dao.getData(Model.User,{_id : userId},{password : 0, __v:0,savedCards : 0,isVerified : 0, isApprovedTutor : 0},{lean: true, limit : 1},callback)
        },
        function(data,callback){
            if(data == null || data.length==0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 400;
                callback(errorResponse);
            } else {
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = data[0];
                var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success)
            }
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null, result);
    })
}

/**
 * Update my profile
 */
exports.updateProfile = function(requestData,callbackRoute){
    var userId = requestData.id;
    delete requestData.id;
    var response = {};
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
         Dao.getData(Model.User,{_id : userId},{mobileNumber : 1},{lean : true, limit : 1},callback);
        },
        function(data,callback){
          if(data.length == 0){
              response.message = Constants.responseMessage.USER_NOT_FOUND;
              response.data = {}
              errorResponse.statusCode = 400
              callback(errorResponse);
          } else if(requestData.profilePicture && requestData.profilePicture.hapi && requestData.profilePicture.hapi.filename){
              FileUtilities.uploadProfilePicture(requestData.profilePicture, data[0].mobileNumber, callback);
          } else {
              callback(null,null);
          }
        },
        function(data,callback) {
            var updateData = {};
            if(data){
                updateData.profilePicture = data.profilePicture;
                updateData.profilePictureThumb = data.profilePictureThumb;
            }
            if(requestData.firstName) updateData.firstName = requestData.firstName;
            if(requestData.lastName) updateData.lastName = requestData.lastName;
            if(requestData.college) updateData.college = requestData.college;
            if(requestData.bioDescription) updateData.bioDescription = requestData.bioDescription;
            if(requestData.password) updateData.password = Util.calculateHash('sha256',requestData.password);
            Dao.updateData(Model.User,{_id : userId},updateData,{},callback);
        },
        function(data,callback){
                response.message = Constants.responseMessage.DATA_UPDATED;
                response.data = {};
                var success = {
                    response : response,
                    statusCode : 201
                }
                callback(null,success);
        }
    ],
    function(err,result){
        if(err) callbackRoute(err);
        else callbackRoute(null,result);
    })
}
/**
 * Available courses at the time of login
 */
exports.availableCourses = function(callbackRoute){
    var response = {}
    var errorResponse = {
        response : response
    }
    Async.waterfall([
        function(callback){
            Dao.getData(Model.Course,{},{code: 1,title:1,desc : 1},{lean : true},callback)
        },
        function(data,callback){
            if(data.length == 0){
                response.message = Constants.responseMessage.NO_DATA_FOUND;
                response.data = {};
                errorResponse.statusCode = 404;
                callback(errorResponse);
            } else {
                response.message = Constants.responseMessage.DATA_FOUND;
                response.data = data;
                var success = {
                    response : response,
                    statusCode : 200
                }
                callback(null,success);
            }
        }
    ],
    function(err,result){
        if(err) callbackRoute(err)
        else callbackRoute(null,result);
    })
}