/**
 * Created by Vikas Sharma on 7/8/15.
 */
/**
 * Created by harekam on 5/15/15.
 */

var constants = require('../utilities/constants');
var Logger = require('../utilities/logger');

/*
 ----------------------------------------
 GET DATA
 ----------------------------------------
 */
exports.getData = function (model, query, projection, options, callback) {

    model.find(query, projection, options, function (err, data) {
        if (err) {
            Logger.error({err : err},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);
        }
        else {
            Logger.debug(data,"Data pulled from the database");
            return callback(null, data);
        }
    });
};

/*
 ----------------------------------------
 GET DISTINCT DATA
 ----------------------------------------
 */
exports.aggregateData = function (model, group, callback) {
    model.aggregate(group, function (err, data) {

        if (err) {
            Logger.error({err : err},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            callback(errResponse);
        }
        else {
            Logger.debug(data,"Aggregated data");
            callback(null, data);
        }
    });
};


/*
 ----------------------------------------
 INSERT DATA
 ----------------------------------------
 */
exports.insertData = function (model, data, callback) {

    new model(data).save(function (err, resultData) {
        if (err) {
            Logger.error({err : err},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            callback(errResponse);
        }
        else {
            //TODO find way to lean save query
            var result = resultData.toObject();
            delete result.__v;
            Logger.debug(result,"Data inserteds in the database");
            callback(null, result);
        }
    });
};

/*
 ----------------------------------------
 UPDATE DATA
 ----------------------------------------
 */
exports.updateData = function (model, conditions, update, options, callback) {

    model.update(conditions, update, options, function (err, result) {
        var errResponse, response;

        if (err) {
            Logger.error({err : err},"Error in database execution");
            response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);
        }
        if (!result) {
            Logger.warn("No data was updated in the database operation");
            response = {
                message: constants.responseMessage.NO_DATA,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);

        }
        if (result.n === 0 || result.nModified === 0) {
            Logger.warn("No data was updated in the database operation");
            response = {
                message: constants.responseMessage.NO_DATA,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);
        }
        Logger.debug(result,"Result of the update operation");
        return callback(null, result);

    });
};

/*
 ----------------------------------------
 DELETE DATA
 ----------------------------------------
 */
exports.deleteData = function (model, conditions, callback) {

    model.remove(conditions, function (err, removed) {
        var errResponse, response;
        if (err) {
            Logger.error({err : err},"Error in database execution");
            response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);
        }
        if (!removed || removed.result.n == 0) {
            Logger.warn("No data was removed from the database");
            response = {
                message: constants.responseMessage.NO_DATA,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);

        }
        Logger.debug(removed.result,"Result of the delete operation");
        return callback(null);
    });
};

/*
 ----------------------------------------
 GET DATA WITH REFERENCE
 ----------------------------------------
 */
exports.getDataWithReference = function (model, query, projection, collectionOptions, callback) {
    model.find(query).select(projection).populate(collectionOptions).exec(function (err, data) {

        if (err) {
            Logger.error({err : err},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            callback(errResponse);
        }
        else {
            Logger.debug(data,"Data with reference fetched from the database");
            callback(null, data);
        }
    });
};

/*
 ----------------------------------------
 BATCH INSERT
 ----------------------------------------
 */
exports.batchInsert = function (model, batchData, callback) {
    model.collection.insert(batchData, function (error, docs) {

        if (error) {
            Logger.error({err : error},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: error,
                statusCode: 400
            };

            callback(errResponse);
        }
        else {
            Logger.debug(docs,"Result of batch insert operation");
            callback(null, docs);
        }
    });
};


exports.getCount = function (model, condition, callback) {
    model.count(condition, function (error, count) {
        if (error) {
            Logger.error({err : err},"Error in database execution");
            var response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            var errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };
            return callback(errResponse);
        }
        Logger.debug(count,"Count of the data pulled from the database");
        return callback(null, count);
    })
};

/*
 ----------------------------------------
 UPDATE DATA And return count
 ----------------------------------------
 */
exports.updateAndReturnCount = function (model, conditions, update, options, callback) {

    model.update(conditions, update, options, function (err, result) {
        var errResponse, response;

        if (err) {
            Logger.error({err : err},"Error in database execution");
            response = {
                message: constants.responseMessage.ERROR_IN_EXECUTION,
                data: {}
            };
            errResponse = {
                response: response,
                details: err,
                statusCode: 400
            };

            return callback(errResponse);
        }
        Logger.trace({res : result}, "Update result");
        return callback(null, result);

    });
};
